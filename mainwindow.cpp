#include "mainwindow.h"
#include "trainingsresultwidget.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QResizeEvent>
#include <QMessageBox>
#include <QDir>
#include <QTimer>
#include <QtWidgets>
#include <QDateTime>
#include <QSound>
#include "widgets/cameraviewwidget.h"
#include "tts/ttstarget.h"
#include <dialogs/timeselectiondialog.h>
#include <tts/ttsdebug.h>
#include <tts/ttsprofiler.h>
#include <tts/ttscommon.h>
#include <tts/ttsresultmanager.h>
#include <tts/ttsleftmenuitem.h>
#include <tts/ttsleftmenu.h>
#include <widgets/cameragrid.h>
#include <widgets/settingswidget.h>
#include <widgets/usertablewidget.h>
#include <db/ttsdb.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Setup stylesheet
    setStyleSheet("QWidget#centralWidget { border-top: 1px solid #112112; }"
                  "QTableView { border: none; }"
                  "QStackedWidget#main { border-left: 1px solid #112112; border-radius: 0px; }"
                  "QToolBar { border-bottom: 1px solid #112112; border-radius: 0px; } ");


    // Initialize application environment
    QCoreApplication::setOrganizationName("itszr.ru");
    QCoreApplication::setOrganizationDomain("itszr.ru");
    QCoreApplication::setApplicationName("LaserBullet");

    setWindowTitle(QString("%1 %2").arg(TTS_APP_NAME).arg(TTS_APP_VERSION));

    settings = new TtsSettings(this);
    cameraSet = new TtsCameraSet(settings, this);
    session = new TtsSession(cameraSet);
    cameraGrid = new CameraGrid(session, this);
    cameraSet->setCameraGrid(cameraGrid);

    stackedWidget = new QStackedWidget;
    stackedWidget->setObjectName("main");

    leftMenu = new TtsLeftMenu(ui->centralWidget);
    connect(leftMenu, &TtsLeftMenu::selectedItemChanged, stackedWidget, &QStackedWidget::setCurrentIndex);

    stackedWidget->addWidget(cameraGrid);
    leftMenu->addItem(tr("Training"), QIcon::fromTheme("view-grid-symbolic"));
    stackedWidget->addWidget(new SettingsWidget(cameraSet));
    leftMenu->addItem(tr("Settings"), QIcon::fromTheme("settings-configure"));
    stackedWidget->addWidget(new UserTableWidget(this));
    leftMenu->addItem(tr("Users"), QIcon::fromTheme("user"));
    stackedWidget->addWidget(new TrainingsResultWidget(this));
    leftMenu->addItem(tr("Results"), QIcon::fromTheme("view-list-text"));
    leftMenu->setActiveMenuItemId(0);

    countdownLabel = new QLabel;
    QFont font = countdownLabel->font();
    font.setPixelSize(100);
    countdownLabel->setFont(font);
    countdownLabel->setAttribute(Qt::WA_TransparentForMouseEvents);

    connect(&countdownTimer, SIGNAL(timeout()), this, SLOT(countdownTimerTriggered()));

    QGridLayout *layout = new QGridLayout;
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(stackedWidget, 0, 0);
    layout->addWidget(countdownLabel, 0, 0);

    QHBoxLayout *mainLayout = new QHBoxLayout(ui->centralWidget);
    mainLayout->setMargin(1);
    mainLayout->setSpacing(0);
    mainLayout->addWidget(leftMenu);
    mainLayout->addLayout(layout);

    QTimer::singleShot(100, [=]() {
        cameraSet->init();
    });
}

MainWindow::~MainWindow()
{
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton btn = QMessageBox::question(this, QCoreApplication::applicationName(), tr("Are you sure?\n"),
                                                            QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                            QMessageBox::Yes);

    if (btn != QMessageBox::Yes) {
        event->ignore();

    } else {
        settings->save();

        if (TtsSession::TTS_STATE_STARTED == session->state())
            session->stop();

        TtsProfiler::printAll();
        event->accept();
    }
}

void MainWindow::showEvent(QShowEvent *event)
{
    QMainWindow::showEvent(event);
    countdownLabel->move((ui->centralWidget->width() - countdownLabel->width()) / 2,
                         (ui->centralWidget->height() - countdownLabel->height()) / 2);
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    countdownLabel->setAlignment(Qt::AlignCenter);
    countdownLabel->setGeometry(0, 0, ui->centralWidget->rect().width(), ui->centralWidget->rect().height());
}

void MainWindow::startSession()
{
    TimeSelectionDialog dlg;
    if (dlg.exec() == QDialog::Accepted) {
        int timeout = dlg.getSessionTime();
        if (timeout > 0) {
            session->prepare(QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss"), timeout);
            countdownValue = 3;
            countdownTimer.start(1000);
        }
    }
}

void MainWindow::countdownTimerTriggered()
{    
    switch (countdownValue--) {
    case 3:
        countdownLabel->show();
        countdownLabel->setText("Ready");
        break;
    case 2:
        countdownLabel->setText("Steady");
        break;
    case 1:
        session->start();
        QSound::play(":/sounds/bell.wav");
        countdownLabel->setText("GO!");
        break;
    case 0:
        disconnect(&countdownTimer);
        countdownTimer.stop();
        countdownLabel->hide();
        break;
    default:
        countdownLabel->setText(QString::number(countdownValue));
        break;
    }
}

void MainWindow::dbusMessage(const QString &arg)
{
    if (arg == "show") {
        this->showNormal();
    }
}

void MainWindow::resetSessionActionTriggered()
{
    session->reset();
}
