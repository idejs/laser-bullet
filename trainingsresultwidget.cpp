#include "trainingsresultwidget.h"

#include <QTableView>
#include <QToolBar>
#include <QVBoxLayout>
#include <QHeaderView>

#include <db/ttsdb.h>

#include <tts/ttsreportbuilder.h>

TrainingsResultWidget::TrainingsResultWidget(QWidget *parent) : QWidget(parent)
{
    model = new TtsTrainingTableModel(TtsTraining::all(this), this);

    toolBar = new QToolBar(this);
    toolBar->setIconSize(QSize(32, 32));

    printAction = toolBar->addAction(QIcon::fromTheme("document-print"), tr("Print"), this, &TrainingsResultWidget::print);
    printPreviewAction = toolBar->addAction(QIcon::fromTheme("document-print-preview"), tr("Print preview"), this, &TrainingsResultWidget::printPreview);

    tableView = new QTableView(this);
    tableView->setModel(model);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    tableView->setTextElideMode(Qt::ElideRight);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(toolBar);
    layout->addWidget(tableView);

    invalidate();
}

void TrainingsResultWidget::invalidate()
{
    model->setItems(TtsTraining::all(this));

    resizeColumns();
}

void TrainingsResultWidget::resizeColumns()
{
    int width = tableView->rect().width();

    tableView->resizeColumnToContents(1);
    tableView->resizeColumnToContents(2);
    tableView->setColumnWidth(0, width - tableView->columnWidth(1) - tableView->columnWidth(2) - tableView->verticalHeader()->width());
}

void TrainingsResultWidget::print()
{
    if (tableView->selectionModel()->selectedRows().size() != 1)
        return;

    QModelIndex index = tableView->selectionModel()->selectedRows().at(0);
    TtsReportBuilder builder(model->itemAt(index.row()));
    builder.print();
}

void TrainingsResultWidget::printPreview()
{
    if (tableView->selectionModel()->selectedRows().size() != 1)
        return;

    QModelIndex index = tableView->selectionModel()->selectedRows().at(0);
    TtsReportBuilder builder(model->itemAt(index.row()));
    builder.printPreview();
}

void TrainingsResultWidget::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    resizeColumns();
}

void TrainingsResultWidget::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);

    invalidate();
}
