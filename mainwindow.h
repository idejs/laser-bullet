#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#ifndef TTS_DEBUG
#define TTS_DEBUG true
#endif

#include <QMainWindow>
#include <QString>
#include <QProcess>
#include <QModelIndex>
#include <QGraphicsView>
#include <QLabel>
#include <QStackedWidget>
#include <QtNetwork/QLocalServer>
#include <widgets/cameragrid.h>
#include <tts/ttsleftmenu.h>
#include <tts/ttssettings.h>
#include "tts/ttscameraset.h"
#include "tts/ttssession.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void startSession();
    void resetSessionActionTriggered();
    void countdownTimerTriggered();

public slots:
    Q_SCRIPTABLE void dbusMessage(const QString &arg);

private:
    Ui::MainWindow *ui;

    CameraGrid *cameraGrid;
    TtsLeftMenu *leftMenu;
    TtsCameraSet *cameraSet;
    TtsSession *session;
    TtsSettings *settings;

    int state;
    int countdownValue;
    bool isActive = false;

    QStackedWidget *stackedWidget;
    QLabel *countdownLabel;
    QTimer countdownTimer;
    QLabel *statusLabel;

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *);
    void showEvent(QShowEvent *event);
    void resizeEvent(QResizeEvent *);
};

#endif // MAINWINDOW_H
