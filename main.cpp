#include "mainwindow.h"
#include <QApplication>
#include <QTextStream>
#include <QMessageBox>
#include <QLockFile>
#include <QSplashScreen>
#include <QDBusConnection>
#include <QDBusInterface>
#include <QLoggingCategory>
#include <QLibraryInfo>
#include "tts/ttsdebug.h"
#include "tts/ttscommon.h"
#include "db/ttsdb.h"

int main(int argc, char *argv[])
{    
    QApplication a(argc, argv);

    // Connect to DBus
    if (!QDBusConnection::sessionBus().isConnected()) {
        TtsDebug::error("Cannot connect to the D-Bus session bus.\n"
                        "To start it, run:\n"
                        "\teval `dbus-launch --auto-syntax`\n");
        return 1;
    }

    // Singleton application
    QLockFile lockFile("/tmp/.lbullet.lock");
    if (!lockFile.tryLock(100)) {
        TtsDebug::warning("Application is already running");

        QDBusInterface iface(TTS_DBUS_SERVICE, "/", "", QDBusConnection::sessionBus());
        if (iface.isValid())
            iface.call("dbusMessage", "show");

        return 1;
    }

    if (!QDBusConnection::sessionBus().registerService(QString(TTS_DBUS_SERVICE))) {
        TtsDebug::error(QDBusConnection::sessionBus().lastError().message());

        return 1;
    }

    QPixmap logo(":/graphics/logo");
    QSplashScreen splash(logo, Qt::WindowStaysOnTopHint);
    splash.show();

    // Opening database instance    
    splash.showMessage("Connecting to database");
    TtsDb::instance()->init();

    // Dummy loading
    QTime dieTime= QTime::currentTime().addSecs(2);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

    splash.hide();

    MainWindow w;
    w.show();

    QDBusConnection::sessionBus().registerObject("/", &w, QDBusConnection::ExportScriptableSlots);

    return a.exec();
}
