#include "timeselectiondialog.h"
#include "ui_timeselectiondialog.h"

#include <QSettings>

TimeSelectionDialog::TimeSelectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TimeSelectionDialog)
{
    ui->setupUi(this);
}

TimeSelectionDialog::~TimeSelectionDialog()
{
    delete ui;
}

int TimeSelectionDialog::getSessionTime()
{
    QTime time = ui->sessionTimeEdit->time();
    return time.minute() * 60 + time.second();
}
