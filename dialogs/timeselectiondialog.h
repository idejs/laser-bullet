#ifndef TIMESELECTIONDIALOG_H
#define TIMESELECTIONDIALOG_H

#include <QDateTime>
#include <QDialog>

namespace Ui {
class TimeSelectionDialog;
}

class TimeSelectionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TimeSelectionDialog(QWidget *parent = 0);
    ~TimeSelectionDialog();

    int getSessionTime();

private:
    Ui::TimeSelectionDialog *ui;
};

#endif // TIMESELECTIONDIALOG_H
