#ifndef TRAININGSRESULTWIDGET_H
#define TRAININGSRESULTWIDGET_H

#include <QTableView>
#include <QToolBar>

#include <models/ttstrainingtablemodel.h>

class TrainingsResultWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TrainingsResultWidget(QWidget *parent = 0);

private:
    TtsTrainingTableModel *model;
    QToolBar *toolBar;
    QTableView *tableView;

    QAction *printPreviewAction;
    QAction *printAction;

    void invalidate();
    void resizeColumns();

signals:

public slots:
    void print();
    void printPreview();

    // QWidget interface
protected:
    void resizeEvent(QResizeEvent *event);
    void showEvent(QShowEvent *event);
};

#endif // TRAININGSRESULTWIDGET_H
