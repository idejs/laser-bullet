#-------------------------------------------------
#
# Project created by QtCreator 2017-01-06T11:45:12
#
#-------------------------------------------------
# Я люблю Малышку Машу

QT       += core gui widgets multimedia printsupport sql dbus xml

TARGET = LaserBullet
TEMPLATE = app

#QMAKE_CXXFLAGS = -std=c++11 -Wall -O2
#QMAKE_CXXFLAGS = -fpermissive -std=c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    widgets/cameragrid.cpp \
    dialogs/timeselectiondialog.cpp \
    tts/ttscamera.cpp \
    tts/ttsframeprocessor.cpp \
    tts/ttsmultishotframeprocessor.cpp \
    tts/ttscameraset.cpp \
    tts/ttscamerasettings.cpp \
    tts/ttssession.cpp \
    widgets/cameraviewwidget.cpp \
    tts/ttstarget.cpp \
    tts/ttsthreadedcameradevice.cpp \
    tts/ttsutils.cpp \
    tts/ttsdebug.cpp \
    tts/ttsprofiler.cpp \
    tts/ttshudbuilder.cpp \
    tts/ttsresultmanager.cpp \
    tts/ttsleftmenu.cpp \
    tts/ttsleftmenuitem.cpp \
    widgets/settingswidget.cpp \
    tts/ttscamerasettingsmodel.cpp \
    ComboBoxDelegate/ComboBoxDelegate.cpp \
    widgets/usertablewidget.cpp \
    models/ttsusertablemodel.cpp \
    db/ttsdb.cpp \
    trainingsresultwidget.cpp \
    models/ttstrainingtablemodel.cpp \
    db/ttstraining.cpp \
    db/ttsuser.cpp \
    db/ttsroom.cpp \
    db/ttsshot.cpp \
    db/querybuilder.cpp \
    db/activerecord.cpp \
    tts/ttsreportbuilder.cpp \
    tts/ttssettings.cpp

HEADERS  += mainwindow.h \
    widgets/cameragrid.h \
    dialogs/timeselectiondialog.h \
    tts/ttscamera.h \
    tts/ttsframeprocessor.h \
    tts/ttsmultishotframeprocessor.h \
    tts/ttscameraset.h \
    tts/ttscamerasettings.h \
    tts/ttssession.h \
    widgets/cameraviewwidget.h \
    tts/ttstarget.h \
    tts/ttsthreadedcameradevice.h \
    tts/ttsutils.h \
    tts/ttsdebug.h \
    tts/ttsprofiler.h \
    tts/ttscommon.h \
    tts/ttshudbuilder.h \
    tts/ttsresultmanager.h \
    tts/ttsleftmenu.h \
    tts/ttsleftmenuitem.h \
    widgets/settingswidget.h \
    tts/ttscamerasettingsmodel.h \
    ComboBoxDelegate/ComboBoxDelegate.h \
    widgets/usertablewidget.h \
    models/ttsusertablemodel.h \
    db/ttsdb.h \
    trainingsresultwidget.h \
    models/ttstrainingtablemodel.h \
    db/ttstraining.h \
    db/ttsuser.h \
    db/ttsroom.h \
    db/ttsshot.h \
    db/querybuilder.h \
    db/activerecord.h \
    tts/ttsreportbuilder.h \
    tts/ttssettings.h

FORMS    += mainwindow.ui \
    dialogs/timeselectiondialog.ui

DISTFILES += \
    README \
    resources/audio/hit.mp3 \
    resources/shots/shot1.gif \
    resources/targets/target2-mask.jpg \
    resources/targets/target2.jpg \
    resources/shots/shot1.png \
    resources/targets/target1.png \
    resources/targets/target2.png \
    resources/targets/target2.pgm \
    qtlogginig.ini

RESOURCES += \
    resources.qrc

LIBS += \
    -L/usr/lib64 \
    -lopencv_core \
    -lopencv_highgui \
    -lopencv_imgproc

INCLUDEPATH += /usr/include
