#include "ttsusertablemodel.h"

#include <db/ttsdb.h>
#include <QBrush>
#include <QFont>
#include <tts/ttsdebug.h>

TtsUserTableModel::TtsUserTableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

TtsUserTableModel::TtsUserTableModel(QMap<int, TtsUser *> items, QObject *parent)
    : QAbstractTableModel(parent)
{
    setItems(items);
}

void TtsUserTableModel::setItems(QMap<int, TtsUser *> items)
{
    beginResetModel();

    foreach (TtsUser *user, m_items.values())
        delete user;

    m_items.clear();
    m_items.unite(items);
    sortItems();

    endResetModel();
}

TtsUser *TtsUserTableModel::itemAt(int i) const
{
    return m_sortedList.value(i);
}

int TtsUserTableModel::itemCount() const
{
    return m_sortedList.size();
}

void TtsUserTableModel::sortItems()
{
    m_sortedList.clear();

    QVector<TtsUser *> deleted;

    foreach (TtsUser *item, m_items) {
        if (item->deleted())
            deleted << item;
        else
            m_sortedList << item;
    }

    m_sortedList << deleted;
}

int TtsUserTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_sortedList.size();
}

int TtsUserTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 5;
}

QVariant TtsUserTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (orientation == Qt::Horizontal) {
            switch (section) {
            case 0: return QVariant();
            case 1: return QVariant(tr("Name"));
            case 2: return QVariant(tr("Trainings"));
            case 3: return QVariant(tr("Scores"));
            case 4: return QVariant(tr("Rating"));
            }
        } else
            return QVariant::fromValue(section + 1);

        break;

    case Qt::FontRole:
        return QFont("Arial", 8);
    }

    return QVariant();
}

QVariant TtsUserTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    TtsUser *item = m_sortedList.at(index.row());

    switch (role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
        if (item) {
            switch (index.column()) {
            case 1: return QVariant(item->name());
            case 2: return QVariant(item->trainings());
            case 3: return QVariant(item->scores());
            case 4: return QVariant(item->trainings() ? item->scores() / item->trainings() : 0.);
            }
        }
        break;

    case Qt::CheckStateRole:
        if (0 == index.column())
            return item->active() ? Qt::Checked : Qt::Unchecked;

        break;

    case Qt::FontRole:
        return QFont("Arial", 8);

    case Qt::ForegroundRole:
        if (TtsUser::STATE_DELETED == item->state())
            return QBrush(Qt::darkGray);

        break;

    case Qt::BackgroundRole:
        if (index.row() % 2)
            return QBrush(QColor(0x22, 0x22, 0x22));

        break;

    case Qt::TextAlignmentRole:
        if (index.column() > 1)
            return Qt::AlignCenter;

        break;
    }

    return QVariant();
}

bool TtsUserTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    TtsUser *item = m_sortedList.at(index.row());

    switch (role) {
    case Qt::EditRole:

        switch (index.column()) {
        case 1:
            item->setName(value.toString());

            emit dataChanged(index, index);
            return true;

        break;
        }

    case Qt::CheckStateRole:
        if (index.column() == 0) {
            item->setState(value.toBool() ? TtsUser::STATE_ACTIVE : TtsUser::STATE_DISABLED);

            emit dataChanged(index, index);
            return true;
        }
    }

    return false;
}

Qt::ItemFlags TtsUserTableModel::flags(const QModelIndex &index) const
{
    TtsUser *item = m_sortedList.at(index.row());

    if (TtsUser::STATE_DELETED == item->state()) {
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    }

    switch (index.column()) {
    case 0: return Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    case 1: return Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    case 2:
    case 3:
    case 4: return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    default: return Qt::ItemIsEnabled;
    }
}
