#ifndef TTSUSERTABLEMODEL_H
#define TTSUSERTABLEMODEL_H

#include <QAbstractTableModel>

#include <db/ttsuser.h>

class TtsUserTableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit TtsUserTableModel(QObject *parent = 0);
    explicit TtsUserTableModel(QMap<int, TtsUser *> items, QObject *parent = 0);

    void setItems(QMap<int, TtsUser *> items);
    TtsUser *itemAt(int i) const;
    int itemCount() const;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;

    Qt::ItemFlags flags(const QModelIndex &index) const override;

private:
    QMap<int, TtsUser *> m_items;
    QVector<TtsUser *> m_sortedList;

protected:
    void sortItems();

};

#endif // TTSUSERTABLEMODEL_H
