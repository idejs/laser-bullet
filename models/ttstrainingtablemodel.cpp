#include "ttstrainingtablemodel.h"
#include "tts/ttscommon.h"

#include <QBrush>
#include <QColor>
#include <QDateTime>
#include <QFont>

TtsTrainingTableModel::TtsTrainingTableModel(QObject *parent) : QAbstractTableModel(parent)
{
}

TtsTrainingTableModel::TtsTrainingTableModel(QVector<TtsTraining *> items, QObject *parent) : QAbstractTableModel(parent)
{
    setItems(items);
}

int TtsTrainingTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_items.size();
}

int TtsTrainingTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 3;
}

QVariant TtsTrainingTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (orientation == Qt::Horizontal) {
            switch (section) {
            case 0: return QVariant(tr("Name"));
            case 1: return QVariant(tr("Start time"));
            case 2: return QVariant(tr("Duration"));
            }
        } else
            return QVariant::fromValue(section + 1);

        break;

    case Qt::FontRole:
        return QFont("Arial", 8);
    }

    return QVariant();
}

QVariant TtsTrainingTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    TtsTraining *item = m_items.at(index.row());

    switch (role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
        if (item) {
            switch (index.column()) {
            case 0: return QVariant(item->name());
            case 1: return QVariant(QDateTime::fromMSecsSinceEpoch(item->timestamp()).toString(TTS_FULL_DATE_FORMAT));
            case 2: return QVariant(item->duration());
            }
        }
        break;

    case Qt::FontRole:
        return QFont("Arial", 8);

    case Qt::BackgroundRole:
        if (index.row() % 2) {
            return QBrush(QColor(0x22, 0x22, 0x22));
        }

    case Qt::TextAlignmentRole:
        if (index.column())
            return Qt::AlignCenter;
    }

    return QVariant();
}

bool TtsTrainingTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    TtsTraining *item = m_items.at(index.row());

    switch (role) {
    case Qt::EditRole:
        switch (index.column()) {
        case 0:
            item->setName(value.toString());

            emit dataChanged(index, index);
            return true;

        break;
        }
    }

    return false;
}

Qt::ItemFlags TtsTrainingTableModel::flags(const QModelIndex &index) const
{
    switch (index.column()) {
    case 0: return Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    case 1:
    case 2: return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    default: return Qt::ItemIsEnabled;
    }
}

void TtsTrainingTableModel::setItems(const QVector<TtsTraining *> &items)
{
    beginResetModel();

    foreach (TtsTraining *item, m_items)
        delete item;

    m_items = items;

    endResetModel();
}

TtsTraining *TtsTrainingTableModel::itemAt(int row) const
{
    return m_items.at(row);
}
