#ifndef TTSTRAININGTABLEMODEL_H
#define TTSTRAININGTABLEMODEL_H

#include <QAbstractTableModel>
#include <db/ttstraining.h>

class TtsTrainingTableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit TtsTrainingTableModel(QObject *parent = 0);
    explicit TtsTrainingTableModel(QVector<TtsTraining *> items, QObject *parent = 0);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    void setItems(const QVector<TtsTraining *> &items);

    TtsTraining *itemAt(int row) const;

private:
    QVector<TtsTraining *> m_items;
};

#endif // TTSTRAININGTABLEMODEL_H
