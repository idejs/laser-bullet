#include "cameraviewwidget.h"

#include <QGridLayout>
#include <QEvent>
#include <QMouseEvent>
#include <QtDebug>
#include <QTimer>
#include <QThread>
#include <QTextDocument>
#include <QTextCursor>
#include <QGraphicsPixmapItem>
#include <QGraphicsProxyWidget>
#include <QDateTime>
#include <QtCore/qmath.h>
#include <tts/ttsdebug.h>
#include <tts/ttsprofiler.h>
#include <tts/ttshudbuilder.h>

CameraViewWidget::CameraViewWidget(TtsCamera *camera, QWidget *parent) :
    QWidget(parent),
    m_camera(camera)
{
    qreal width = 640.;
    qreal height = 480.;

    graphicsScene = new QGraphicsScene(this);
    graphicsScene->setSceneRect(0., 0., width, height);

    graphicsView = new QGraphicsView(graphicsScene);
    graphicsView->setStyleSheet("QGraphicsView { background: #000; border: 1px solid #111;}");
    graphicsView->installEventFilter(this);
    graphicsView->setRenderHint(QPainter::Antialiasing);

    videoItem = new QGraphicsPixmapItem();
    graphicsScene->addItem(videoItem);

    targetItem = new QGraphicsPixmapItem();
    graphicsScene->addItem(targetItem);

    centerText = new QGraphicsTextItem();
    centerText->setFont(QFont("Tahoma", 16));
    graphicsScene->addItem(centerText);

    hudBuilder = new TtsHudBuilder(graphicsScene, this);
    hudBuilder->build(30, 8);

    QGridLayout *layout = new QGridLayout(this);
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(graphicsView, 0, 0, 1, 1);

    fitInView();
}

CameraViewWidget::~CameraViewWidget()
{
    delete videoItem;
    delete targetItem;
    delete centerText;
    delete graphicsScene;
    delete graphicsView;
}

void CameraViewWidget::setVideoFrame(const QImage &image)
{
    videoItem->setPixmap(QPixmap::fromImage(image));
}

void CameraViewWidget::setVideoVisible(bool visible)
{
    videoItem->setVisible(visible);
}

void CameraViewWidget::setTargetImage(QImage image)
{
    targetImage = image;
    targetItem->setPixmap(QPixmap::fromImage(targetImage));
}

void CameraViewWidget::setTargetVisible(bool visible)
{
    targetItem->setVisible(visible);
}

bool CameraViewWidget::targetVisible() const
{
    return targetItem->isVisible();
}

void CameraViewWidget::setCenterText(const QString &text)
{
    centerText->setPlainText(text);
    centerText->setTextWidth(640);

    QTextBlockFormat format;
    format.setAlignment(Qt::AlignCenter);
    centerText->textCursor().setBlockFormat(format);

    centerText->setPos(320 - centerText->textWidth() / 2, 220);
    centerText->show();
}

void CameraViewWidget::setCenterTextVisible(bool visible)
{
    centerText->setVisible(visible);
}

void CameraViewWidget::setCameraName(const QString &name)
{
    hudBuilder->setNameValue(name);
}

void CameraViewWidget::setPoints(uint points)
{
    m_points = points;
    hudBuilder->setPointsValue(points);
}

void CameraViewWidget::setId(int id)
{
    m_id = id;
}

TtsHudBuilder *CameraViewWidget::hud() const
{
    return hudBuilder;
}

void CameraViewWidget::fitInView()
{
    if (graphicsView == 0)
        return;

    graphicsView->fitInView(graphicsScene->sceneRect(), Qt::KeepAspectRatio);
}

int CameraViewWidget::id() const
{
    return m_id;
}

void CameraViewWidget::setCamera(TtsCamera *camera)
{
    m_camera = camera;
}

TtsCamera *CameraViewWidget::camera() const
{
    return m_camera;
}

bool CameraViewWidget::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonDblClick) {
        QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
        if (mouseEvent->button() == Qt::LeftButton) {
            emit mouseDoubleClick(this);
        }
    } else if (event->type() == QEvent::MouseButtonPress) {
        QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
        if (mouseEvent->button() == Qt::RightButton) {
            emit contextMenu(this);
        }
    }

    return QWidget::eventFilter(object, event);
}

void CameraViewWidget::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);
    fitInView();
}
