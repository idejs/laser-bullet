#ifndef CAMERAVIEWWIDGET_H
#define CAMERAVIEWWIDGET_H

class TtsCamera;

#include "tts/ttshudbuilder.h"

#include <QLabel>
#include <QGraphicsView>
#include <QTimer>
#include <QSlider>
#include <QGraphicsProxyWidget>
#include <opencv2/opencv.hpp>

class CameraViewWidget : public QWidget
{
    Q_OBJECT

public:
    CameraViewWidget(TtsCamera *camera = 0, QWidget *parent = 0);
    ~CameraViewWidget();

    QGraphicsView *view() const { return graphicsView; }

    void setVideoFrame(const QImage &image);
    void setVideoVisible(bool visible);
    void setTargetImage(QImage image);    
    void setCenterText(const QString &text);
    void setCenterTextVisible(bool visible);
    void setCameraName(const QString &name);
    void setPoints(uint points);
    void setId(int id);
    TtsHudBuilder* hud() const;

    void setCounter(int seconds);
    void fitInView();
    int id() const;

    void setCamera(TtsCamera *camera);
    TtsCamera *camera() const;

    void setTargetVisible(bool visible);
    bool targetVisible() const;

signals:
    void mouseDoubleClick(CameraViewWidget *object);
    void contextMenu(CameraViewWidget *object);

private:
    TtsCamera *m_camera;

    int m_id;
    uint m_points = 0;

    TtsHudBuilder *hudBuilder;

    QGraphicsPixmapItem *targetItem;
    QGraphicsPixmapItem *videoItem;
    QGraphicsTextItem *centerText;
    QGraphicsView *graphicsView = 0;
    QGraphicsScene *graphicsScene;
    QImage targetImage;


    bool isFirstShow = true;
    QTimer timer;
    int secondsLeft = 0;

public:
    bool eventFilter(QObject *, QEvent *);

    // QWidget interface
protected:
    void paintEvent(QPaintEvent *event);
};

#endif // CAMERAVIEWWIDGET_H
