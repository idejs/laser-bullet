#include "cameragrid.h"

#include <QtMath>
#include <QIcon>
#include <QAction>
#include <QMenu>
#include <QFileDialog>

#include <tts/ttscamera.h>
#include <tts/ttscameraset.h>
#include <tts/ttsresultmanager.h>
#include "tts/ttsdebug.h"
#include <widgets/cameraviewwidget.h>
#include <mainwindow.h>

CameraGrid::CameraGrid(TtsSession *session,  QWidget *parent) :
    QWidget(parent)
{   
    setSession(session);
    cameraSet = session->cameraSet();

    toolBar = new QToolBar(this);
    toolBar->setIconSize(QSize(32, 32));

    gridView = new QWidget(this);

    cameraView = new CameraViewWidget;
    connect(cameraView, &CameraViewWidget::mouseDoubleClick, [=]() {
        viewGrid();
    });

    stackedWidget = new QStackedWidget;
    stackedWidget->addWidget(gridView);
    stackedWidget->addWidget(cameraView);
    stackedWidget->setCurrentIndex(0);

    mainLayout = new QVBoxLayout;
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);
    mainLayout->addWidget(toolBar);
    mainLayout->addWidget(stackedWidget);

    layout = new QGridLayout(gridView);
    layout->setMargin(0);
    layout->setSpacing(0);

    setLayout(mainLayout);

    createToolBarActions();
}

void CameraGrid::setSession(TtsSession *session)
{
    if (this->session)
        disconnect(this->session);

    this->session = session;
    connect(session, &TtsSession::stateChanged, [=](int) {
        updateToolBarActions();
    });
}

void CameraGrid::setCameraSet(TtsCameraSet *cameraSet)
{
    this->cameraSet = cameraSet;
}

void CameraGrid::addCameraWidget(CameraViewWidget *widget)
{
    widgets.append(widget);

    connect(widget, &CameraViewWidget::mouseDoubleClick, this, &CameraGrid::dblClickTriggered);
}

CameraViewWidget *CameraGrid::getWidget(int i)
{
    return widgets.value(i);
}

int CameraGrid::widgetCount()
{
    return widgets.size();
}

void CameraGrid::updateGrid()
{
    int count = widgetCount();
    int cols = calcGridColums(count);

    for (int i = 0; i < count; i++) {
        CameraViewWidget *widget = getWidget(i);
        widget->setParent(0);
        layout->addWidget(widget, i / cols, i % cols);
    }
}

int CameraGrid::calcGridColums(int count)
{
//    return qSqrt(count);
    if (count < 2)
        return 1;
    else if (count < 5)
        return 2;
    else if (count < 10)
        return 3;
    else
        return 4;
}

void CameraGrid::createToolBarActions()
{
    MainWindow *mw = static_cast<MainWindow*>(parent());

    startSessionAction = toolBar->addAction(QIcon::fromTheme("media-playback-start"), tr("Старт"), mw, &MainWindow::startSession);
    stopSessionAction = toolBar->addAction(QIcon::fromTheme("media-playback-stop"), tr("Остановить"), session, &TtsSession::stop);
    resetSessionAction = toolBar->addAction(QIcon::fromTheme("entry-delete"), tr("Сбросить"), mw, &MainWindow::resetSessionActionTriggered);

    toolBar->addSeparator();

    printSessionResultAction = toolBar->addAction(QIcon::fromTheme("document-print"), tr("Печать"), cameraSet, &TtsCameraSet::printSessionResult);
    previewSessionResultAction = toolBar->addAction(QIcon::fromTheme("document-preview"), tr("Предпросмотр"), cameraSet, &TtsCameraSet::previewSessionResult);

    toolBar->addSeparator();

    screenshotAction = toolBar->addAction(QIcon::fromTheme("folder-pictures-symbolic"), tr("Screenshot"), cameraSet, &TtsCameraSet::makeScreenshot);

    exportSessionResultAction = toolBar->addAction(QIcon::fromTheme("document-export"), tr("Export"), [=]() {
        QString filename = QFileDialog::getSaveFileName(this, tr("Save result"), QString("%1.xml").arg(session->name()), tr("*.xml"));
        if (!filename.isNull()) {
            TtsResultManager exporter;
            exporter.exportSession(session, filename);
        }
    });

    adjustLighteningLevelAction = toolBar->addAction(QIcon::fromTheme("adjustlevels"), tr("Calibrate lightening"));
    adjustLighteningLevelAction->setCheckable(true);
    connect(adjustLighteningLevelAction, &QAction::toggled, [=](bool toogled){
        if (cameraSet)
            cameraSet->setCalibrationEnabled(toogled);

        updateToolBarActions();
    });

    toolBar->addSeparator();

    closeAction = toolBar->addAction(QIcon::fromTheme("application-exit"), tr("Выход"), [=]() {
        mw->close();
    });

    updateToolBarActions();
}

void CameraGrid::updateToolBarActions()
{
    TtsDebug::debug("CameraGrid::updateToolBarActions");

    int state = session->state();

    startSessionAction->setEnabled(TtsSession::TTS_STATE_PREVIEW == state && !adjustLighteningLevelAction->isChecked());
    stopSessionAction->setEnabled(TtsSession::TTS_STATE_STARTED == state && !adjustLighteningLevelAction->isChecked());
    resetSessionAction->setEnabled(TtsSession::TTS_STATE_RESULT == state && !adjustLighteningLevelAction->isChecked());
    printSessionResultAction->setEnabled(TtsSession::TTS_STATE_RESULT == state && !adjustLighteningLevelAction->isChecked());
    previewSessionResultAction->setEnabled(TtsSession::TTS_STATE_RESULT == state && !adjustLighteningLevelAction->isChecked());
    screenshotAction->setEnabled((TtsSession::TTS_STATE_PREVIEW == state && TtsSession::TTS_STATE_RESULT == state) && !adjustLighteningLevelAction->isChecked());
    exportSessionResultAction->setEnabled(TtsSession::TTS_STATE_RESULT == state && !adjustLighteningLevelAction->isChecked());
    adjustLighteningLevelAction->setEnabled(TtsSession::TTS_STATE_PREVIEW == state);

}

void CameraGrid::dblClickTriggered(CameraViewWidget *widget)
{
    cameraSet->selectCamera(widget->camera());
    viewCamera();
}

void CameraGrid::viewCamera()
{
    TtsCamera *camera = cameraSet->selectedCamera();

    if (camera && camera->settings()->enabled()) {
        camera->pushWidget(cameraView);
        stackedWidget->setCurrentIndex(1);
    }
}

void CameraGrid::viewGrid()
{
    cameraSet->selectedCamera()->restoreWidget();
    stackedWidget->setCurrentIndex(0);
}
