#include "usertablewidget.h"

#include <QToolBar>
#include <QAction>
#include <QVBoxLayout>
#include <QHeaderView>

#include <db/ttsdb.h>

#include <tts/ttsdebug.h>

UserTableWidget::UserTableWidget(QWidget *parent) : QWidget(parent)
{
    init();
}

void UserTableWidget::init()
{
    m_model = new TtsUserTableModel(this);

    m_toolBar = new QToolBar(this);
    m_toolBar->setIconSize(QSize(32, 32));
    createActions();

    m_tableView = new QTableView;
    m_tableView->setModel(m_model);
    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    m_tableView->setTextElideMode(Qt::ElideRight);
    m_tableView->setEditTriggers(QAbstractItemView::AnyKeyPressed |
                                   QAbstractItemView::DoubleClicked |
                                   QAbstractItemView::SelectedClicked);
    m_tableView->show();

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setSpacing(0);
    layout->setMargin(0);
    layout->addWidget(m_toolBar);
    layout->addWidget(m_tableView);

    setLayout(layout);

    invalidate();
}

void UserTableWidget::createActions()
{
    m_toolBar->addAction(QIcon::fromTheme("list-add-user"), tr("Add user"), [=]() {
        QString name = QString(tr("User %1")).arg(m_model->itemCount() + 1);
        TtsUser user(name);

        invalidate();
        m_tableView->scrollToBottom();
    });

    m_toolBar->addAction(QIcon::fromTheme("list-remove-user"), tr("Remove user"), [=]() {
        foreach (QModelIndex index, m_tableView->selectionModel()->selectedRows()) {
            m_model->itemAt(index.row())->setState(TtsUser::STATE_DELETED);
        }

        invalidate();
    });

}

void UserTableWidget::invalidate()
{
    m_model->setItems(TtsUser::map(m_model));

    resizeColumns();
}

void UserTableWidget::resizeColumns()
{
    int width = m_tableView->rect().width();

    m_tableView->resizeColumnToContents(0);
    m_tableView->resizeColumnToContents(2);
    m_tableView->resizeColumnToContents(3);
    m_tableView->resizeColumnToContents(4);
    m_tableView->setColumnWidth(1, width - m_tableView->columnWidth(0) - m_tableView->columnWidth(2) -
                                m_tableView->columnWidth(3) - m_tableView->columnWidth(4) -
                                m_tableView->verticalHeader()->width());
}

void UserTableWidget::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    resizeColumns();
}

void UserTableWidget::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    invalidate();
}
