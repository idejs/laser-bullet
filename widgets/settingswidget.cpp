#include "settingswidget.h"

#include <tts/ttsdebug.h>
#include <tts/ttstarget.h>

#include <QHeaderView>

SettingsWidget::SettingsWidget(TtsCameraSet *cameraSet, QWidget *parent) : QWidget(parent)
{
    setCameraSet(cameraSet);

    m_model = new TtsCameraSettingsModel(cameraSet);

    ComboBoxDelegate *targetComboBox = new ComboBoxDelegate(this);
    foreach (TtsTarget target, TtsTarget::list())
        targetComboBox->addItem(target.name());

    userComboBox = new ComboBoxDelegate(this);
    updateUserComboBox();

    m_cameraTable = new QTableView;
    m_cameraTable->setItemDelegateForColumn(2, targetComboBox);
    m_cameraTable->setItemDelegateForColumn(3, userComboBox);
    m_cameraTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_cameraTable->setSelectionMode(QAbstractItemView::SingleSelection);
    m_cameraTable->setTextElideMode(Qt::ElideRight);
    m_cameraTable->setEditTriggers(QAbstractItemView::AnyKeyPressed |
                                   QAbstractItemView::DoubleClicked |
                                   QAbstractItemView::SelectedClicked);
    m_cameraTable->show();

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setSpacing(0);
    layout->setMargin(0);
    layout->addWidget(m_cameraTable);

    setLayout(layout);
}

SettingsWidget::~SettingsWidget()
{
    delete m_model;
}

TtsCameraSet *SettingsWidget::cameraSet() const
{
    return m_cameraSet;
}

void SettingsWidget::setCameraSet(TtsCameraSet *cameraSet)
{
    m_cameraSet = cameraSet;
}

void SettingsWidget::invalidate()
{
    m_cameraTable->setModel(m_model);

    m_cameraTable->resizeColumnsToContents();
    m_cameraTable->resizeRowsToContents();
    m_cameraTable->setColumnWidth(2, 150);
    m_cameraTable->setColumnWidth(3, 150);

    QHeaderView *horizontalHeaderView = m_cameraTable->horizontalHeader();
    horizontalHeaderView->setStretchLastSection(true);

    updateUserComboBox();
}

void SettingsWidget::updateUserComboBox()
{
    userComboBox->clear();
    userComboBox->addItem(TtsUser().name());
    foreach (TtsUser *user, TtsUser::all(TtsUser::STATE_ACTIVE, this))
        userComboBox->addItem(user->name());
}

void SettingsWidget::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    invalidate();
}
