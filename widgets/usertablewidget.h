#ifndef USERTABLEWIDGET_H
#define USERTABLEWIDGET_H

#include <QObject>
#include <QTableView>
#include <QToolBar>

#include <models/ttsusertablemodel.h>

class UserTableWidget : public QWidget
{
    Q_OBJECT
public:
    explicit UserTableWidget(QWidget *parent = 0);

private:
    void init();
    void createActions();
    void invalidate();
    void resizeColumns();

private:
    QTableView *m_tableView;
    TtsUserTableModel *m_model;
    QToolBar *m_toolBar;

signals:

    // QWidget interface
protected:
    void resizeEvent(QResizeEvent *event);
    void showEvent(QShowEvent *event);

    // QWidget interface
protected:

};

#endif // USERTABLEWIDGET_H
