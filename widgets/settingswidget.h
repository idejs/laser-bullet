#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <tts/ttscameraset.h>
#include <tts/ttscamerasettingsmodel.h>

#include <QTableView>

#include <ComboBoxDelegate/ComboBoxDelegate.h>


class SettingsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsWidget(TtsCameraSet *cameraSet, QWidget *parent = 0);
    virtual ~SettingsWidget();

    TtsCameraSet *cameraSet() const;
    void setCameraSet(TtsCameraSet *cameraSet);

private:
    TtsCameraSet *m_cameraSet;
    TtsCameraSettingsModel *m_model;
    QTableView *m_cameraTable;
    ComboBoxDelegate *userComboBox;

    void invalidate();
    void updateUserComboBox();

signals:

public slots:

    // QWidget interface
protected:
    void showEvent(QShowEvent *event);
};

#endif // SETTINGSWIDGET_H
