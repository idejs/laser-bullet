#ifndef CAMERAGRID_H
#define CAMERAGRID_H

class TtsCamera;
class TtsCameraSet;
class TtsSession;
class CameraDialog;
class CameraViewWidget;

#include <QGridLayout>
#include <QList>
#include <QStackedWidget>
#include <QToolBar>

class CameraGrid : public QWidget
{
    Q_OBJECT
public:
    explicit CameraGrid(TtsSession *aSession, QWidget *parent = 0);

    void setSession(TtsSession *session);
    void setCameraSet(TtsCameraSet *cameraSet);
    void addCameraWidget(CameraViewWidget *widget);
    CameraViewWidget *getWidget(int i);
    int widgetCount();

    void updateGrid();

    void viewCamera();
    void viewGrid();


protected:
    int calcGridColums(int count);

    void createToolBarActions();
    void updateToolBarActions();

private:
    TtsCameraSet *cameraSet;
    TtsSession *session;
    CameraDialog *dialog;
    CameraViewWidget *cameraView;

    QToolBar *toolBar;
    QStackedWidget *stackedWidget;
    QVBoxLayout *mainLayout;
    QGridLayout *layout;
    QList<CameraViewWidget *> widgets;
    QWidget *gridView;

    QAction *startSessionAction;
    QAction *stopSessionAction;
    QAction *resetSessionAction;
    QAction *printSessionResultAction;
    QAction *previewSessionResultAction;
    QAction *screenshotAction;
    QAction *exportSessionResultAction;
    QAction *adjustLighteningLevelAction;
    QAction *closeAction;

signals:

public slots:
    void dblClickTriggered(CameraViewWidget *widget);
};

#endif // CAMERAGRID_H
