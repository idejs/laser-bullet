#include "ttssettings.h"

#include <QSettings>

TtsSettings::TtsSettings(QObject *parent) : QObject(parent)
{
    load();
}

TtsSettings::~TtsSettings()
{
    save();
}

void TtsSettings::load()
{
    QSettings settings;
    settings.beginGroup("General");
    m_cameraCount = settings.value("Count", 0).toInt();
    settings.endGroup();
}

void TtsSettings::save()
{
    QSettings settings;
    settings.beginGroup("General");
    settings.setValue("Count", QVariant::fromValue(m_cameraCount));
    settings.endGroup();
}

int TtsSettings::cameraCount() const
{
    return m_cameraCount;
}

void TtsSettings::setCameraCount(int cameraCount)
{
    m_cameraCount = cameraCount;
}
