#include "ttshudbuilder.h"

#include <QDateTime>
#include <QTextBlockFormat>
#include <QTextCursor>

TtsHudBuilder::TtsHudBuilder(QGraphicsScene *scene, QObject *parent) : QObject(parent)
{
    this->scene = scene;
}

void TtsHudBuilder::build(qreal offset, qreal corners, QPen pen)
{
    this->offset = offset;

    // build frame
    buildFrame(offset, corners, pen);

    QBrush whiteBrush(Qt::white);
    QFont nameFont("Arial", 16, 50);

    QTextBlockFormat format; format.setAlignment(Qt::AlignCenter);

    nameItem = scene->addSimpleText("Camera name", nameFont);
    nameItem->setBrush(whiteBrush);
    nameItem->setPos(offset + 5, scene->height() - offset - 25);

    timeItem = scene->addText("", nameFont);

    pointsItem = scene->addText("", QFont("Arial", 30));

    shotsItem = scene->addSimpleText("", QFont("Arial", 16));
    shotsItem->setBrush(whiteBrush);
    shotsItem->setPos(offset + 8, offset + 8);

    // Crazy mother fucker love. First - Viktory, Second - Masha, 3 - Sasha ???????????????
    m_slider = new QSlider;
    m_slider->setOrientation(Qt::Horizontal);
    m_slider->setTickInterval(1);
    m_slider->setMaximum(255);
    m_slider->setStyleSheet("QSlider { background: transparent; } ");
//    connect(m_slider, &QSlider::valueChanged, this, &TtsHudBuilder::changeLighteningLevel);

    sliderWidget = scene->addWidget(m_slider);
    sliderWidget->setPos(10, scene->height() - sliderWidget->rect().height() - 5);
    QSizeF size = sliderWidget->size(); size.setWidth(scene->width() - 20);
    sliderWidget->resize(size);
    sliderWidget->setVisible(false);

    setTimeValue(1);
    setPointsValue(10);
}

void TtsHudBuilder::setNameValue(QString name)
{
    nameItem->setText(name);
}

void TtsHudBuilder::setTimeValue(qint64 time)
{
    setTimeVisible(true);
    QTextBlockFormat format; format.setAlignment(Qt::AlignCenter);

    timeItem->setPlainText(QDateTime::fromMSecsSinceEpoch(time).toString("mm:ss"));
    timeItem->setTextWidth(scene->width());
    timeItem->textCursor().setBlockFormat(format);
    timeItem->setPos(scene->width() / 2 - timeItem->textWidth() / 2, offset + 10);
}

void TtsHudBuilder::setPointsValue(qint32 points)
{
    setPointsVisible(points != 0);
    if (points == 0)
        return;

    QTextBlockFormat format; format.setAlignment(Qt::AlignCenter);

    pointsItem->setPlainText(QString("%1").arg(points));
    pointsItem->setTextWidth(scene->width());
    pointsItem->textCursor().setBlockFormat(format);
    pointsItem->setPos(scene->width() / 2  - timeItem->textWidth() / 2, scene->height() - offset - 75);
}

void TtsHudBuilder::setShotsValue(quint32 shots)
{
    setShotsVisisble(shots != 0);
    shotsItem->setText(QString("%1").arg(shots));
}

void TtsHudBuilder::setNameVisisble(bool visible)
{
    m_nameVisible = visible;
    updateVisibility();
}

void TtsHudBuilder::setTimeVisible(bool visible)
{
    m_timeVisible = visible;
    updateVisibility();
}

void TtsHudBuilder::setPointsVisible(bool visible)
{
    m_pointsVisible = visible;
    updateVisibility();
}

void TtsHudBuilder::setFrameVisible(bool visible)
{
    m_frameVisible = visible;
    updateVisibility();
}

void TtsHudBuilder::setShotsVisisble(bool visible)
{
    m_shotsVisible = visible;
    updateVisibility();
}

void TtsHudBuilder::show()
{
    m_visible = true;
    updateVisibility();
}

void TtsHudBuilder::hide()
{
    m_visible = false;
    updateVisibility();
}

void TtsHudBuilder::setSliderVisible(bool visible)
{
    sliderWidget->setVisible(visible);
}

bool TtsHudBuilder::sliderVisible() const
{
    return sliderWidget->isVisible();
}

void TtsHudBuilder::buildFrame(qreal offset, qreal corner, QPen pen)
{
    qreal frameLength = 100;

    // top-left
    frameItems << scene->addLine(offset + corner, offset, offset + frameLength * 2, offset, pen);
    frameItems << scene->addLine(offset, offset + corner, offset, offset + frameLength, pen);

    // right-bottom
    frameItems << scene->addLine(scene->width() - offset - corner, offset, scene->width() - offset - frameLength * 2, offset, pen);
    frameItems << scene->addLine(scene->width() - offset, offset + corner, scene->width() - offset, offset + frameLength, pen);

    // left-bottom
    frameItems << scene->addLine(offset + corner, scene->height() - offset, offset + frameLength * 2, scene->height() - offset, pen);
    frameItems << scene->addLine(offset, scene->height() - offset - corner, offset, scene->height() - offset - frameLength, pen);

    // top-right
    frameItems << scene->addLine(scene->width() - offset - corner, scene->height() - offset, scene->width() - offset - frameLength * 2, scene->height() - offset, pen);
    frameItems << scene->addLine(scene->width() - offset, scene->height() - offset - corner, scene->width() - offset, scene->height() - offset - frameLength, pen);
}

void TtsHudBuilder::updateVisibility()
{
    foreach (QGraphicsLineItem *item, frameItems) {
        item->setVisible(m_visible && m_frameVisible);
    }

    nameItem->setVisible(m_visible && m_nameVisible);
    timeItem->setVisible(m_visible && m_timeVisible);
    pointsItem->setVisible(m_visible && m_pointsVisible);
    shotsItem->setVisible(m_visible && m_shotsVisible);
}

QSlider *TtsHudBuilder::slider() const
{
    return m_slider;
}
