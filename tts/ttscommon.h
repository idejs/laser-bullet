#ifndef TTSCOMMON_H
#define TTSCOMMON_H

#define TTS_DBUS_SERVICE "ru.itszr.tts.DBus"
#define TTS_APP_NAME "Laser Bullet"
#define TTS_APP_VERSION "0.8.1b"
#define TTS_MAX_CAMS 16
#define TTS_CAM_NAME_TEMPLATE "CAM%1"
#define TTS_FULL_DATE_FORMAT "yyyy-MM-dd hh:mm:ss"

#endif // TTSCOMMON_H
