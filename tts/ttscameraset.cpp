#include "ttstarget.h"
#include "ttscameraset.h"
#include "ttsreportbuilder.h"
#include "ttsdebug.h"
#include "widgets/cameraviewwidget.h"

#include <QPrintDialog>
#include <QPrintPreviewDialog>
#include <QtDebug>

#include <db/ttsdb.h>
#include <tts/ttscommon.h>

TtsCameraSet::TtsCameraSet(TtsSettings *settings, QObject *parent) : QObject(parent)
{
    m_settings = settings;
}

void TtsCameraSet::init()
{
    if (m_list.size() > 0)
        return;

    TtsDebug::debug("Total camera count: " + QString::number(m_settings->cameraCount()));

    foreach (TtsCameraSettings *settings, TtsCameraSettings::all(m_settings->cameraCount(), this))
        addCamera(settings);

    updateGrid();
}

TtsCamera* TtsCameraSet::addCamera(TtsCameraSettings *settings)
{
    Q_ASSERT(settings);

    TtsCamera *camera = new TtsCamera(settings, this);
    m_list.append(camera);
    return camera;
}

void TtsCameraSet::setCameraGrid(CameraGrid *grid)
{
    m_cameraGrid = grid;

    if (grid) {
        grid->setCameraSet(this);
        updateGrid();
    }
}

void TtsCameraSet::prepareTraining()
{
    for (int i = 0; i < m_list.size(); i++) {
        TtsCamera *camera = m_list[i];

        camera->widget()->hud()->hide();

        if (camera->settings()->enabled()) {
//            camera->widget()->setTargetVisible(false);
            camera->widget()->hud()->show();
            camera->prepare();
        }
    }
}

void TtsCameraSet::startTraining(TtsSession *session)
{
    m_session = session;

    TtsTraining *training = session->training();

    foreach (TtsCamera *camera, m_list) {
        if (camera->enabled()) {
            TtsUser user(camera->settings()->userId());
            TtsRoom *room = new TtsRoom(training, &user, camera->settings()->id(),
                                        camera->settings()->target());
            camera->startSession(session, room);
        }
    }
}

void TtsCameraSet::stopTraining()
{
    foreach (TtsCamera *camera, m_list) {
        if (camera->enabled())
            camera->stopSession();
    }
}

void TtsCameraSet::resetTraining()
{
    foreach (TtsCamera *camera, m_list) {
        if (camera->settings()->enabled())
            camera->reset();

        camera->widget()->hud()->show();
    }
}

void TtsCameraSet::makeScreenshot()
{
    foreach (TtsCamera *camera, m_list) {
        if (camera->settings()->enabled())
            emit camera->makeScreenshot();
    }
}

TtsCamera *TtsCameraSet::camera(int id)
{
    for (int i = 0; i < m_list.size(); i++) {
        if (m_list[i]->settings()->id() == id)
            return m_list[i];
    }

    return 0;
}

const QVector<TtsCamera *> TtsCameraSet::list() const
{
    return m_list;
}

void TtsCameraSet::setCalibrationEnabled(bool enabled)
{
    foreach (TtsCamera *camera, m_list) {
        if (camera->active())
            camera->setCalibrationEnabled(enabled);
    }
}

void TtsCameraSet::selectCamera(TtsCamera *camera)
{
    m_selectedCamera = camera;
}

TtsCamera *TtsCameraSet::selectedCamera()
{
    return m_selectedCamera;
}

void TtsCameraSet::addCameraWidget(CameraViewWidget *)
{

}

void TtsCameraSet::updateGrid()
{
    if (m_cameraGrid == 0)
        return;

    for (int i = 0; i < m_list.size(); i++) {
        TtsCamera *camera = m_list[i];

        CameraViewWidget *widget = new CameraViewWidget(camera);

        camera->setWidget(widget);
        m_cameraGrid->addCameraWidget(widget);
    }

    m_cameraGrid->updateGrid();
}

void TtsCameraSet::printSessionResult()
{
    TtsReportBuilder builder(m_session->training());
    builder.print();
}

void TtsCameraSet::previewSessionResult()
{
    if (TtsSession::TTS_STATE_RESULT != m_session->state())
        return;

    TtsReportBuilder builder(m_session->training());
    builder.printPreview();
}
