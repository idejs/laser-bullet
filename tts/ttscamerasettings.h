﻿#ifndef TTSCAMERASETTINGS_H
#define TTSCAMERASETTINGS_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QObject>
#include <QString>

#include <db/activerecord.h>

class TtsCameraSettings : public ActiveRecord
{
    Q_OBJECT
public:
    static const QString ID;
    static const QString NAME;
    static const QString SOURCE;
    static const QString ENABLED;
    static const QString TARGET;
    static const QString USER;

    explicit TtsCameraSettings(int id = -1, QObject *parent = 0);
    explicit TtsCameraSettings(QSqlQuery &query, QObject *parent = 0);
    explicit TtsCameraSettings(QString source, QString target = "", bool enabled = false, QString name = "", QObject *parent = 0);

    static void createTable();
    static QVector<TtsCameraSettings *> all(int max = 0, QObject *parent = 0);

signals:

public slots:

    // ActiveRecord interface
public:
    const QString tableName() const;
    const QStringList columns() const;

    int id() const;

    QString name() const;
    void setName(const QString &name);

    QString source() const;
    void setSource(const QString &source);

    bool enabled() const;
    void setEnabled(bool enabled);

    QString target() const;
    void setTarget(const QString &target);

    int userId() const;
    void setUserId(int userId);

protected:
    bool loadEntity(const QSqlQuery &query);
    bool createEntity();
    bool updateEntity();

private:
    int m_id;
    QString m_name;
    QString m_source;
    bool m_enabled;
    QString m_target;
    int m_userId = 0;
};

#endif // TTSCAMERASETTINGS_H
