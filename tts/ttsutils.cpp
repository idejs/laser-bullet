#include "ttsutils.h"

TtsUtils::TtsUtils()
{

}

QImage TtsUtils::cvToImage(cv::Mat &source, int cvtFormat, QImage::Format imgFormat = QImage::Format_RGB888)
{
    cv::Mat dest;
    cv::cvtColor(source, dest, cvtFormat);
    return QImage((uchar*) dest.data, dest.cols, dest.rows, dest.step1(), imgFormat);
}

bool TtsUtils::cvToFile(cv::Mat source, int cvtFormat, QImage::Format imgFormat, QString filename)
{
    QImage image = cvToImage(source, cvtFormat, imgFormat);
    return image.save(filename);
}
