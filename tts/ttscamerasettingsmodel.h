#ifndef TTSCAMERASETTINGSMODEL_H
#define TTSCAMERASETTINGSMODEL_H

#include "ttscameraset.h"

#include <QAbstractTableModel>

class TtsCameraSettingsModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit TtsCameraSettingsModel(TtsCameraSet *cameraSet, QObject *parent = 0);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    TtsCameraSet *cameraSet() const;
    void setCameraSet(TtsCameraSet *cameraSet);

private:
    TtsCameraSet *m_cameraSet;
};

#endif // TTSCAMERASETTINGSMODEL_H
