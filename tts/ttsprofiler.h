#ifndef TTSPROFILER_H
#define TTSPROFILER_H

#ifndef TTS_DEBUG
#define TTS_DEBUG true
#endif

#include <QString>
#include <QMap>

class TtsProfiler : public QObject
{
private:
    TtsProfiler() {}

    static bool enabled;
    static QMap<QString, qint64> dataMap;
    static QMap<QString, qint64> passesMap;
    static QMap<QString, qint64> activeMap;

public:
    static void setEnabled(bool enabled);
    static void start(const QString &token);
    static qint64 stop(const QString &token);
    static qint64 passes(const QString &token);
    static qint64 ticks(const QString &token);
    static qint64 average(const QString &token);
    static QString result(QString &token);
    static void printAll();


signals:

public slots:
};

#endif // TTSPROFILER_H
