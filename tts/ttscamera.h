#ifndef TTSCAMERA_H
#define TTSCAMERA_H

#include <QObject>
#include <QPrinter>

#include <db/ttsroom.h>

#include "ttsmultishotframeprocessor.h"
#include "ttssession.h"
#include "ttsthreadedcameradevice.h"

#define SAVE_SCREENSHOTS false

class TtsCamera: public QObject
{
    Q_OBJECT
public:
    explicit TtsCamera(TtsCameraSettings *settings, QObject *parent = 0);
    virtual ~TtsCamera();

    TtsCameraSettings *settings() const;
    CameraViewWidget *widget() const;

    void prepare();

    void reset();
    void update();

    void print();
    void printPreview();

    void setWidget(CameraViewWidget *widget);
    void pushWidget(CameraViewWidget *widget);
    void restoreWidget();

    void setEnabled(bool enabled);

    void startSession(TtsSession *session, TtsRoom *room);
    void stopSession();
    void resetSession();

    void enable();
    void disable();

    bool resultShowing();
    bool active() const;
    bool enabled() const;
    bool lost() const;

    void updateShots();
    int sessionPoints();
    int sessionShots();

    void setTemporaryDisabled(bool value);
    void setCalibrationEnabled(bool enable);

    TtsRoom *room() const;

private:
    TtsRoom *m_room;
    TtsSession *m_session = 0;
    qint64 m_sessionStartTime;
    QImage shotImage;
    QThread *thread;
    TtsThreadedCameraDevice *cameraDevice;
    TtsMultiShotFrameProcessor *frameProcessor;
    TtsCameraSettings *cameraSettings;
    CameraViewWidget *cameraWidget;
    CameraViewWidget *storedCameraWidget;
    QImage targetImage;
    QImage screenshotImage;
    bool isCapturing;
    bool isRayDetection;
    bool temporaryDisabled = false;

    void startCapture();
    void stopCapture();

protected:
    void updateDevice();
    void updateWidget();
    int calculatePoints(int x, int y, int minPoints = 5);
    void drawShot(QPainter *painter, const QPoint &point);
    void drawShot(QImage *targetImg, const QPoint &point);

signals:
    void deviceCalibrated(bool calibrated);
    void stopThread();
    void makeScreenshot();
    void changeDeviceSource(const QString source);

public slots:
    void rayDetected(const QRect &shotPoint);
    void multiRayDetected(const QVector<QRect> points);
    void filterCalibrated(bool adjusted);

    void handleScreenshot(const QImage frame);
    void handleFrame(const QImage frame);
    void deviceStateChanged(int);
};

#endif // TTSCAMERA_H
