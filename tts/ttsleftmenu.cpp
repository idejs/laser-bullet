#include "ttsleftmenu.h"
#include "ttsdebug.h"

#include <QIcon>
#include <QGraphicsSimpleTextItem>
#include <QGraphicsPixmapItem>
#include <QTextBlockFormat>
#include <QImage>
#include <QStyleOption>
#include <QMouseEvent>

TtsLeftMenu::TtsLeftMenu(QWidget *parent) : QWidget(parent)
{
    m_items = new QVector<TtsLeftMenuItem *>();

    layout = new QBoxLayout(QBoxLayout::TopToBottom);
    layout->setMargin(0);
    layout->setSpacing(0);
    setLayout(layout);

    setFixedWidth(80);
//    setStyleSheet("TtsLeftMenu { border: 1px solid #000; }");
}

TtsLeftMenu::~TtsLeftMenu()
{
    delete m_items;
}

void TtsLeftMenu::addItem(TtsLeftMenuItem *item)
{
    TtsDebug::debug("TtsLeftMenu::addItem(TtsLeftMenuItem *item)");

//    item->setParent(this);
    m_items->append(item);
    layout->addWidget(item);

    connect(item, &TtsLeftMenuItem::menuItemSelected, this, &TtsLeftMenu::menuItemSelected);
}

TtsLeftMenuItem *TtsLeftMenu::addItem(const QString title, const QIcon &icon)
{
    TtsDebug::debug("TtsLeftMenu::addItem(const QString title, const QIcon &icon)");

    TtsLeftMenuItem *item = new TtsLeftMenuItem(title, icon, this);
    addItem(item);
    return item;
}

void TtsLeftMenu::activateMenuItem(TtsLeftMenuItem *item)
{
    m_activeMenuItem = item;

    for (int i = 0; i < m_items->size(); i++) {
        TtsLeftMenuItem *currentItem = m_items->at(i);
        currentItem->setActiveItem(false);
    }

    m_activeMenuItem->setActiveItem(true);

    if (m_stackedWidget)
        m_stackedWidget->setCurrentIndex(m_items->indexOf(m_activeMenuItem));
}

TtsLeftMenuItem *TtsLeftMenu::activeMenuItem() const
{
    return m_activeMenuItem;
}

void TtsLeftMenu::setActiveMenuItemId(int id)
{
    if (id >= 0 && id < m_items->size())
        activateMenuItem(m_items->at(id));
}

void TtsLeftMenu::setStackedWidget(QStackedWidget *widget)
{
    m_stackedWidget = widget;
}

void TtsLeftMenu::menuItemSelected(TtsLeftMenuItem *item)
{
    TtsDebug::debug("TtsLeftMenu::menuItemSelected");
    activateMenuItem(item);
    emit selectedItemChanged(m_items->indexOf(item));
}



void TtsLeftMenu::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    resize(80, m_items->size() * 60);
}


void TtsLeftMenu::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);

    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
