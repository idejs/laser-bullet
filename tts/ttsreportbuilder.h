#ifndef TTSREPORTBUILDER_H
#define TTSREPORTBUILDER_H

#include <QObject>
#include <QPrinter>

#include <db/ttsroom.h>
#include <db/ttstraining.h>

class TtsReportBuilder : public QObject
{
    Q_OBJECT
public:
    explicit TtsReportBuilder(TtsTraining *training, QObject *parent = 0);

    void print();
    void printPreview();

private:
    TtsTraining *m_training;

    Q_INVOKABLE void paintResults(QPrinter *printer);
    void paintRoomResult(const TtsRoom *room, QPainter &painter, QPrinter *printer);

signals:

public slots:

};

#endif // TTSREPORTBUILDER_H
