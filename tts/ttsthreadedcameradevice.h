#ifndef TTSTHREADEDCAMERADEVICE_H
#define TTSTHREADEDCAMERADEVICE_H

#include "ttsmultishotframeprocessor.h"

#include <opencv2/opencv.hpp>

#include <QImage>
#include <QThread>
#include <QTimer>

class TtsThreadedCameraDevice : public QObject
{
    Q_OBJECT
public:
    enum State {
        STATE_UNCONFIGURED,
        STATE_CONFIGURED,
        STATE_CLOSED,
        STATE_STOPED,
        STATE_CONNECTING,
        STATE_CONNECTION_LOST,
        STATE_CAPTURING,
        STATE_ERROR,
        STATE_LOST
    };

    explicit TtsThreadedCameraDevice(int id, const QString &source, TtsMultiShotFrameProcessor *frameProcessor, QObject *parent = 0);
    bool opened();
    int state();

private:
    QMutex stateMutex;
    QMutex mutex;
    QTimer *captureTimer;
    cv::VideoCapture *device = 0;
    cv::Mat preparedBgrFrame;
    QString m_source;
    TtsMultiShotFrameProcessor *processor;
    volatile int m_state;
    int m_id;
    int m_maxRetries = 3;
    int m_retries = 0;
    int m_timeout = 1000;
    int m_captureTimeout = 50;
    int m_makeScreenshot = false;
    int m_timerId = 0;

    bool open();
    Q_SLOT bool capturing() const;
    void setState(int state);
    bool setSource(const QString &source);

    QImage convertToImage(cv::Mat &preparedBgrFrame);
    void captureFrame();
    cv::Mat prepareFrame(const cv::Mat source);
    void release();

    void connectionEstablished();
    void connectionLost();

    QString textState(int state = -1);

signals:
    void frameReady(const QImage frame);
    void screenshotReady(const QImage frame);
    void finished();
    void stateChanged(int state);

public slots:
    void changeSource(const QString source);
    void capture();
    void stop();
    void screenshot();
};

#endif // TTSTHREADEDCAMERADEVICE_H
