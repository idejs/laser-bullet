#include "ttsdebug.h"

#include <QDateTime>
#include <QtDebug>

TtsDebug::TtsDebug(QObject *parent) : QObject(parent)
{

}

void TtsDebug::message(QString &msg, QString level = "DEBUG")
{
    QString dtStr = QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss:zzz");
    qDebug() << QString("%1: %2 - %3").arg(dtStr).arg(level).arg(msg);
}

QString TtsDebug::levelToStr(int level)
{
    switch (level) {
    case INFO: return "INFO";
    case DEBUG: return "DEBUG";
    case WARNING: return "WARNING";
    case ERROR: return "ERROR";
    default: return "UNKNOWN";
    }
}

void TtsDebug::debug(QString msg)
{
    message(msg, levelToStr(DEBUG));
}

void TtsDebug::info(QString msg)
{
    message(msg, levelToStr(INFO));
}

void TtsDebug::error(QString msg)
{
    message(msg, levelToStr(ERROR));
}

void TtsDebug::warning(QString msg)
{
    message(msg, levelToStr(WARNING));
}
