#include "ttscamera.h"
#include "ttstarget.h"
#include "ttsmultishotframeprocessor.h"
#include "ttsthreadedcameradevice.h"
#include "ttsprofiler.h"
#include "ttsdebug.h"
#include "ttscommon.h"
#include "ttsreportbuilder.h"
#include "widgets/cameraviewwidget.h"

#include <QDateTime>
#include <QPrintDialog>
#include <QPrintPreviewDialog>
#include <QThread>
#include <QtDebug>
#include <QtGlobal>
#include <QtMath>
#include <QLatin1Char>
#include <QRgb>
#include <QSound>

#include <db/ttsdb.h>

TtsCamera::TtsCamera(TtsCameraSettings *settings, QObject *parent) : QObject(parent),
    cameraSettings(settings),
    cameraWidget(0)
{
    cameraSettings = settings;
    cameraWidget  = 0;

    isRayDetection = false;

    shotImage = QImage(":/graphics/shots/shot-red.png");

    thread = new QThread;

    frameProcessor = new TtsMultiShotFrameProcessor;
    frameProcessor->setEnabled(false);
    frameProcessor->setTrackPointsEnabled(false);
    connect(frameProcessor, &TtsMultiShotFrameProcessor::rayDetected, this, &TtsCamera::rayDetected);
    connect(frameProcessor, &TtsMultiShotFrameProcessor::filterCalibrated, this, &TtsCamera::filterCalibrated);

    cameraDevice = new TtsThreadedCameraDevice(settings->id(), settings->source(), frameProcessor);
    cameraDevice->moveToThread(thread);

    connect(thread, &QThread::started, cameraDevice, &TtsThreadedCameraDevice::capture);
    connect(thread, &QThread::finished, cameraDevice, &TtsThreadedCameraDevice::deleteLater);

    connect(cameraDevice, &TtsThreadedCameraDevice::frameReady, this, &TtsCamera::handleFrame);
    connect(cameraDevice, &TtsThreadedCameraDevice::screenshotReady, this, &TtsCamera::handleScreenshot);
    connect(cameraDevice, &TtsThreadedCameraDevice::stateChanged, this, &TtsCamera::deviceStateChanged);

    connect(this, &TtsCamera::stopThread, cameraDevice, &TtsThreadedCameraDevice::stop);
    connect(this, &TtsCamera::makeScreenshot, cameraDevice, &TtsThreadedCameraDevice::screenshot);
    connect(this, &TtsCamera::changeDeviceSource, cameraDevice, &TtsThreadedCameraDevice::changeSource);

    thread->start();

    reset();
}

TtsCamera::~TtsCamera()
{
    if (thread->isRunning()) {
        emit stopThread();
        thread->quit();
        thread->wait();
    }

    delete thread;
}

TtsCameraSettings *TtsCamera::settings() const
{
    return cameraSettings;
}

CameraViewWidget *TtsCamera::widget() const
{
    return cameraWidget;
}

void TtsCamera::prepare()
{
//    m_hitCollection.clear();
//    m_hitCollection.reset();

    if (cameraWidget) {
        cameraWidget->setTargetVisible(true);
        cameraWidget->hud()->setTimeVisible(false);
        cameraWidget->hud()->setPointsVisible(false);
    }

    if (frameProcessor->getLigteningLevel() == 0)
        frameProcessor->calibrate();
}

void TtsCamera::filterCalibrated(bool adjusted)
{
    emit deviceCalibrated(adjusted);
}

void TtsCamera::handleScreenshot(const QImage frame)
{
    QString filename = QString("tts/cam%1-%2.jpg").arg(settings()->name()).arg(QDateTime::currentDateTime().toMSecsSinceEpoch());
    screenshotImage = frame.copy(frame.rect());
    screenshotImage.save(filename);
}

void TtsCamera::reset()
{
//    m_hitCollection.clear();

    targetImage = QImage(TtsTarget(cameraSettings->target()).targetPath());

    if (cameraWidget != 0) {
        cameraWidget->setTargetImage(targetImage);
        cameraWidget->hud()->setTimeVisible(false);
    }

    startCapture();
    update();
}

void TtsCamera::update()
{
    updateDevice();
    updateWidget();
}

void TtsCamera::print()
{
    if (!m_session || m_session->state() != TtsSession::TTS_STATE_RESULT)
        return;

    TtsReportBuilder builder(m_session->training());
    builder.print();
}

void TtsCamera::printPreview()
{
    TtsReportBuilder builder(m_session->training());
    builder.printPreview();
}

void TtsCamera::setWidget(CameraViewWidget *widget)
{
    bool sliderVisible = false;
    bool targetVisible = false;

    if (cameraWidget) {
        disconnect(cameraWidget);
        sliderVisible = cameraWidget->hud()->sliderVisible();
        targetVisible = cameraWidget->targetVisible();
    }

    cameraWidget = widget;

    updateWidget();

    if (cameraWidget) {
        cameraWidget->setTargetImage(targetImage);
        cameraWidget->setId(cameraSettings->id());
        cameraWidget->setTargetVisible(targetVisible);
        cameraWidget->hud()->setSliderVisible(sliderVisible);

        connect(widget->hud()->slider(), &QSlider::valueChanged, frameProcessor, &TtsMultiShotFrameProcessor::setLaigheningLevel);
        connect(frameProcessor, &TtsMultiShotFrameProcessor::lighteningLevelChanged, widget->hud()->slider(), &QSlider::setValue);

        QMetaObject::invokeMethod(widget->hud()->slider(), "setValue", Qt::QueuedConnection, Q_ARG(int, frameProcessor->getLigteningLevel()));
    }
}

void TtsCamera::pushWidget(CameraViewWidget *widget)
{
    storedCameraWidget = cameraWidget;
    setWidget(widget);
}

void TtsCamera::restoreWidget()
{
    setWidget(storedCameraWidget);
}

void TtsCamera::setEnabled(bool enabled)
{
    cameraSettings->setEnabled(enabled);

    if (enabled)
        startCapture();
    else
        stopCapture();

    updateDevice();
    updateWidget();
}

void TtsCamera::startSession(TtsSession *session, TtsRoom *room)
{
    m_session = session;
    m_room = room;

    reset();

    cameraWidget->setTargetVisible(true);
    frameProcessor->setEnabled(true);

    emit makeScreenshot();
}

void TtsCamera::stopSession()
{
    stopCapture();
    frameProcessor->setEnabled(false);
    cameraWidget->setVideoFrame(screenshotImage);

    if (m_room->userId()) {
        TtsUser user(m_room->userId());
        user.addTrainingResult(m_room->points() * 1. / m_room->shotCount());
    }

    QMetaObject::invokeMethod(frameProcessor, "saveResult", Qt::QueuedConnection);
}

void TtsCamera::resetSession()
{
    m_session = 0;
}

void TtsCamera::startCapture()
{
    isCapturing = true;
}

void TtsCamera::stopCapture()
{
    isCapturing = false;
}

void TtsCamera::enable()
{
    setEnabled(true);
}

void TtsCamera::disable()
{
    setEnabled(false);
}

bool TtsCamera::resultShowing()
{
    return m_session != 0 && m_session->state() == TtsSession::TTS_STATE_RESULT;
}

bool TtsCamera::active() const
{
    return enabled() && cameraDevice->opened();
}

bool TtsCamera::enabled() const
{
    return cameraSettings->enabled() && !temporaryDisabled;
}

bool TtsCamera::lost() const
{
    return temporaryDisabled;
}

int TtsCamera::sessionPoints()
{
    return m_room->points();
}

int TtsCamera::sessionShots()
{
    return m_room->shotCount();
}

void TtsCamera::setTemporaryDisabled(bool value)
{
    temporaryDisabled = value;
}

void TtsCamera::setCalibrationEnabled(bool enable)
{
    if (enable)
        frameProcessor->calibrate(1);

    frameProcessor->setCalibrationModeEnabled(enable);
    widget()->setTargetVisible(!enable);
    widget()->hud()->setSliderVisible(enable);
    widget()->hud()->setTimeVisible(!enable);
}

TtsRoom *TtsCamera::room() const
{
    return m_room;
}

void TtsCamera::updateDevice()
{
    emit changeDeviceSource(cameraSettings->source());
}

void TtsCamera::updateWidget()
{
    if (!cameraWidget)
        return;

    if (!cameraSettings->name().isEmpty())
        cameraWidget->setCameraName(cameraSettings->name());
    else
        cameraWidget->setCameraName(QString(TTS_CAM_NAME_TEMPLATE).arg(cameraSettings->id()));

    cameraWidget->setTargetImage(targetImage);

    if (m_session && m_session->active() && active() && sessionPoints()) {
        cameraWidget->setPoints(sessionPoints());
    } else {
        cameraWidget->setPoints(0);
    }

    if (resultShowing()) {
        cameraWidget->setVideoFrame(screenshotImage);
        cameraWidget->setPoints(sessionPoints());
        cameraWidget->setCenterTextVisible(false);
        return;
    }

    cameraWidget->setTargetVisible(cameraSettings->enabled());
    cameraWidget->setVideoVisible(cameraSettings->enabled());
    cameraWidget->setCenterTextVisible(false);

    cameraWidget->hud()->setTimeVisible(enabled());
    cameraWidget->setTargetVisible(enabled());
    cameraWidget->setVideoVisible(enabled());

    setTemporaryDisabled(TtsThreadedCameraDevice::STATE_LOST == cameraDevice->state());

    if (lost()) {
        cameraWidget->setCenterText(tr("CAMERA LOST"));
        return;
    }

    if (!enabled()) {
        cameraWidget->setCenterText(tr("CAMERA DISABLED"));
        return;
    }

    int deviceState = cameraDevice->state();
    switch (deviceState) {
//    case TtsThreadedCameraDevice::STATE_LOST:
//        setTemporaryDisabled(true);
//        break;

    case TtsThreadedCameraDevice::STATE_UNCONFIGURED:
        cameraWidget->setCenterText(tr("UNCONFIGURED"));
        cameraWidget->setTargetVisible(false);
        cameraWidget->setVideoVisible(false);
        cameraWidget->hud()->setTimeVisible(false);
        return;

    case TtsThreadedCameraDevice::STATE_CONNECTING:
    case TtsThreadedCameraDevice::STATE_CONNECTION_LOST:
        cameraWidget->setCenterText(tr("CONNECTING"));
        cameraWidget->setVideoVisible(false);
        cameraWidget->setTargetVisible(false);
        cameraWidget->hud()->setTimeVisible(false);
        return;

    case TtsThreadedCameraDevice::STATE_ERROR:
        cameraWidget->setCenterText(tr("CONNECTION LOST"));
        cameraWidget->setVideoVisible(false);
        cameraWidget->setTargetVisible(false);
        cameraWidget->hud()->setTimeVisible(false);
        return;
    }
}

int TtsCamera::calculatePoints(int x, int y, int minPoints)
{
    int distance = qSqrt(qPow(qAbs((320 - x)), 2) + qPow(qAbs((240 - y)), 2));
    int points = 10 - distance / (120 / 5);
    return qMax(minPoints, points);
}

void TtsCamera::drawShot(QPainter *painter, const QPoint &point)
{
    if (!painter)
        return;

    int x = point.x() - (shotImage.width() / 2);
    int y = point.y() - (shotImage.height() / 2);
    painter->drawImage(x, y, shotImage);
}

void TtsCamera::drawShot(QImage *targetImg, const QPoint &point)
{
    QPainter painter(targetImg);
    drawShot(&painter, point);
    painter.end();
}

void TtsCamera::rayDetected(const QRect &rect)
{
    if (!isCapturing || !m_session || !m_session->active())
        return;

    TtsTarget target(settings()->target());
    QImage maskImage(target.maskPath());

    int points = 0;
    if (maskImage.pixel(rect.center()) == 0xFFFFFFFF) {
        points = calculatePoints(rect.center().x(), rect.center().y());

        if (active() && m_room->points() + points) {
            cameraWidget->hud()->setPointsValue(m_room->points() + points);
        }
        QSound::play(":/sounds/ding.wav");
    }

    qint64 timestamp = QDateTime::currentDateTime().currentMSecsSinceEpoch() - m_session->startTime();
    m_room->addShot(rect.center(), timestamp, points);

    cameraWidget->hud()->setShotsValue(m_room->shotCount());

    drawShot(&targetImage, rect.center());
    cameraWidget->setTargetImage(targetImage);
}

void TtsCamera::multiRayDetected(const QVector<QRect> detections)
{
    if (detections.size() > 0)
        rayDetected(detections[0]);
}

void TtsCamera::handleFrame(const QImage frame)
{
    if (isCapturing && settings()->enabled() && cameraWidget != 0) {
        cameraWidget->setVideoFrame(frame);

        if (m_session && m_session->active())
            cameraWidget->hud()->setTimeValue(QDateTime::currentDateTime().currentMSecsSinceEpoch() - m_session->startTime());
    }
}

void TtsCamera::deviceStateChanged(int)
{
    updateWidget();
}
