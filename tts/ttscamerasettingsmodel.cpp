#include "ttscamerasettingsmodel.h"
#include "ttscommon.h"
#include "ttsdebug.h"
#include "ttstarget.h"

TtsCameraSettingsModel::TtsCameraSettingsModel(TtsCameraSet *cameraSet, QObject *parent) : QAbstractTableModel(parent),  m_cameraSet(cameraSet) { }

int TtsCameraSettingsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return cameraSet()->list().size();
}

int TtsCameraSettingsModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 5;
}

QVariant TtsCameraSettingsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
        if (orientation == Qt::Horizontal) {
            switch (section) {
            case 0: return QVariant(tr(""));
            case 1: return QVariant(tr("Name"));
            case 2: return QVariant(tr("Target"));
            case 3: return QVariant(tr("User"));
            case 4: return QVariant(tr("Source"));
            }
        } else
            return QVariant(QString(TTS_CAM_NAME_TEMPLATE).arg(section + 1));

        break;

    case Qt::FontRole:
        return QFont("Arial", 8);
    }

    return QVariant();
}

QVariant TtsCameraSettingsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    TtsCamera *camera = cameraSet()->camera(index.row()+1);
    TtsCameraSettings *settings = camera->settings();

    switch (role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
        if (settings) {
            switch (index.column()) {
            case 1: return QVariant(settings->name());
            case 2: return TtsTarget::getTarget(settings->target()).name();
            case 3: return QVariant(TtsUser(settings->userId()).name()); // Optimizations from firends
            case 4: return QVariant(settings->source());
            }
        }
        break;

    case Qt::CheckStateRole:
        if (0 == index.column())
            return settings->enabled() ? Qt::Checked : Qt::Unchecked;
        break;

    case Qt::FontRole:
        return QFont("Arial", 10);
    }

    return QVariant();
}

bool TtsCameraSettingsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    TtsCamera *camera = cameraSet()->camera(index.row()+1);
    TtsCameraSettings *settings = camera->settings();

    switch (role) {
    case Qt::EditRole:

        switch (index.column()) {
        case 1:
            settings->setName(value.toString());
            camera->reset();
            emit dataChanged(index, index);
            return true;

        case 2:
            settings->setTarget(TtsTarget::findTarget(value.toString()).id());
            camera->reset();
            emit dataChanged(index, index);
            return true;

        case 3:
            settings->setUserId(TtsUser::find(value.toString()).id());
            emit dataChanged(index, index);
            return true;

        case 4:
            settings->setSource(value.toString());
            camera->setTemporaryDisabled(false);
            camera->reset();
            emit dataChanged(index, index);
            return true;
        }

        break;

    case Qt::CheckStateRole:
        if (index.column() == 0) {
            settings->setEnabled(value.toBool());
            camera->update();
            emit dataChanged(index, index);
            return true;
        }
    }

    return false;
}

Qt::ItemFlags TtsCameraSettingsModel::flags(const QModelIndex &index) const
{
    switch (index.column()) {
    case 0: return Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    case 1:
    case 2:
    case 3:
    case 4: return Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    default: return Qt::ItemIsEnabled;
    }
}

TtsCameraSet *TtsCameraSettingsModel::cameraSet() const
{
    return m_cameraSet;
}

void TtsCameraSettingsModel::setCameraSet(TtsCameraSet *cameraSet)
{
    m_cameraSet = cameraSet;
}
