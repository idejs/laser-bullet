#include "ttsresultmanager.h"

#include <QDateTime>
#include <QFile>
#include <QMessageBox>

TtsResultManager::TtsResultManager(QObject *parent) : QObject(parent)
{

}

void TtsResultManager::exportSession(TtsSession *session, QString filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::warning(0, tr("Export result"), tr("Failed opening file for writing"), QMessageBox::Ok);
        return;
    }

    QXmlStreamWriter writer(&file);
    writer.setCodec("UTF-8");
    writer.setAutoFormatting(true);
    writer.writeStartDocument();

    writer.writeStartElement("session");
    writer.writeAttribute("name", session->name());
    writer.writeAttribute("start", QDateTime::fromMSecsSinceEpoch(session->startTime()).toString(Qt::ISODate));

    const QVector<TtsCamera *> list = session->cameraSet()->list();

    foreach (TtsCamera *camera, list) {
        if (camera->active())
            writeCameraInfo(writer, camera);
    }

    writer.writeEndElement();
    writer.writeEndDocument();
    file.close();
}

void TtsResultManager::writeCameraInfo(QXmlStreamWriter &writer, TtsCamera *camera)
{
    writer.writeStartElement("camera");
    writer.writeAttribute("id", QString::number(camera->settings()->id()));
    writer.writeAttribute("name", camera->settings()->name());
    writer.writeAttribute("source", camera->settings()->source());
    writer.writeAttribute("target", camera->settings()->target());

    if (camera->settings()->userId())
        writer.writeAttribute("user", TtsUser(camera->settings()->userId()).name());

    // TODO: writeHitsInfo
//    writeHitsInfo(writer, &camera->);

    writer.writeEndElement();
}

//void TtsResultManager::writeHitsInfo(QXmlStreamWriter &writer, TtsHitCollection *hits)
//{
//    writer.writeStartElement("hits");
//    QVector<TtsHit> hitList = hits->hitList();

//    foreach (TtsHit hit, hitList) {
//        writer.writeStartElement("hit");
//        writer.writeAttribute("time", QDateTime::fromMSecsSinceEpoch(hit.time()).toString("mm:ss.zzz"));
//        writer.writeAttribute("point", QString::number(hit.points()));
//        writer.writeAttribute("x", QString::number(hit.x()));
//        writer.writeAttribute("y", QString::number(hit.y()));
//        writer.writeEndElement();
//    }

//    writer.writeEndElement();
//}
