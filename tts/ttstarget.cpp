#include "ttstarget.h"

#include <QDirIterator>
#include <QtDebug>

QString TtsTarget::path = ":/graphics/targets/";
QList<TtsTarget> TtsTarget::targetList = QList<TtsTarget>();

TtsTarget::TtsTarget(const QString &id)
{
    m_id = id;
    m_targetPath = path + m_id + ".png";
    m_maskPath = path + m_id + ".pgm";

    QFile descFile(path + m_id + ".desc");
    descFile.open(QIODevice::ReadOnly);
    m_name = descFile.readLine();
}

QList<TtsTarget> TtsTarget::list()
{
    if (TtsTarget::targetList.size() == 0) {
        QStringList filter; filter << "*.desc";

        QDirIterator it(path, filter, QDir::Files);
        while (it.hasNext()) {
            QString descFileName = it.next();            
            QString id = descFileName.mid(descFileName.lastIndexOf('/') + 1);
            id = id.left(id.lastIndexOf('.'));
            TtsTarget::targetList.append(TtsTarget(id));
        }
    }

    return TtsTarget::targetList;
}

TtsTarget TtsTarget::getTarget(const QString &id)
{
    if (id.trimmed().length() == 0)
        return TtsTarget::defaultTarget();

    QList<TtsTarget> targets = list();
    for (int i = 0; i < targets.size(); i++) {
        if (id == targets.at(i).id())
            return targets.at(i);
    }

    return targets[0];
}

TtsTarget TtsTarget::findTarget(const QString &name)
{
    QList<TtsTarget> targetList = TtsTarget::list();

    foreach (TtsTarget target, targetList) {
        if (name == target.name())
            return target;
    }

    return targetList.at(0);
}

TtsTarget TtsTarget::defaultTarget()
{
    return TtsTarget::list().at(0);
}

QString TtsTarget::id() const
{
    return m_id;
}

QString TtsTarget::name() const
{
    return m_name;
}

QString TtsTarget::targetPath() const
{
    return m_targetPath;
}

QString TtsTarget::maskPath() const
{
    return m_maskPath;
}
