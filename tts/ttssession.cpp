#include "ttstarget.h"
#include "ttssession.h"

#include <QDateTime>
#include <QtDebug>
#include <QSound>
#include <QException>

#include <db/ttsdb.h>

TtsSession::TtsSession(TtsCameraSet *set, QObject *parent) : QObject(parent)
{
    m_state = TTS_STATE_PREVIEW;
    m_cameraSet = set;
}


/**
 * @brief TtsSession::prepare Prepare session internal camera set states
 * @param timeout Timeout of session
 * @return True if session prepared
 */
bool TtsSession::prepare(const QString &name, int timeout)
{
    if (state() != TTS_STATE_PREVIEW)
        return false;

    m_name = name;
    m_timeout = timeout;

    setState(TTS_STATE_PREPARE);
    m_cameraSet->prepareTraining();
    setState(TTS_STATE_PREPARED);
    return true;
}

bool TtsSession::start()
{
    if (state() != TTS_STATE_PREPARED)
        return false;

    m_startTime = QDateTime::currentDateTime().currentMSecsSinceEpoch();
    m_training = new TtsTraining(m_name, m_startTime, m_timeout);

    m_timeoutTimerId = startTimer(m_timeout * 1000, Qt::PreciseTimer);
    m_cameraSet->startTraining(this);

    setState(TTS_STATE_STARTED);
    return true;
}

bool TtsSession::stop()
{
    if (state() != TTS_STATE_STARTED)
        return false;

    killTimer(m_timeoutTimerId);
    m_cameraSet->stopTraining();

    QSound::play(":/sounds/bell.wav");

    setState(TTS_STATE_RESULT);
    return false;
}

bool TtsSession::reset()
{
    if (state() != TTS_STATE_RESULT)
        return false;

    m_cameraSet->resetTraining();
    setState(TTS_STATE_PREVIEW);
    return true;
}

bool TtsSession::active() const
{
    return state() == TTS_STATE_STARTED;
}

qint64 TtsSession::startTime() const
{
    return m_startTime;
}

QString TtsSession::name() const
{
    return m_name;
}

int TtsSession::state() const
{
    return m_state;
}

void TtsSession::setState(const int &value)
{
    if (m_state == value)
        return;

    m_state = value;
    emit stateChanged(m_state);
}

TtsTraining *TtsSession::training() const
{
    return m_training;
}

void TtsSession::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);
    stop();
}

int TtsSession::timeout() const
{
    return m_timeout;
}

TtsCameraSet *TtsSession::cameraSet() const
{
    return m_cameraSet;
}
