#ifndef TTSCAMERASET_H
#define TTSCAMERASET_H

class TtsSession;
class CameraViewWidget;
class CameraGrid;

#include "ttscamerasettings.h"
#include "ttssettings.h"
#include "widgets/cameragrid.h"
#include "tts/ttscamera.h"

#include <QDateTime>
#include <QObject>
#include <QVector>
#include <QPrinter>

class TtsCameraSet : public QObject
{
    Q_OBJECT
public:
    explicit TtsCameraSet(TtsSettings *settings, QObject *parent = 0);

    void init();

    TtsCamera* addCamera(TtsCameraSettings *settings);
    void setCameraGrid(CameraGrid *grid);

    void prepareTraining();
    void startTraining(TtsSession *session);
    void stopTraining();
    void resetTraining();

    void selectCamera(TtsCamera *camera);
    TtsCamera *selectedCamera();

    TtsCamera *camera(int id);
    const QVector<TtsCamera*> list() const;

    void setCalibrationEnabled(bool enabled);

private:
//    TtsCameraManager *m_manager;
    TtsSession *m_session;
    TtsCamera *m_selectedCamera;
    CameraGrid *m_cameraGrid;
    TtsSettings *m_settings;

    QVector<TtsCamera *> m_list;

    void addCameraWidget(CameraViewWidget *);
    void updateGrid();

signals:

public slots:
    void printSessionResult();
    void previewSessionResult();
    void makeScreenshot();
};

#endif // TTSCAMERASET_H
