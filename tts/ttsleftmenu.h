#ifndef TTSLEFTMENU_H
#define TTSLEFTMENU_H

#include "ttsleftmenuitem.h"

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLabel>
#include <QObject>
#include <QVBoxLayout>
#include <QWidget>
#include <QVector>
#include <QStackedWidget>


class TtsLeftMenu : public QWidget
{
    Q_OBJECT
public:
    explicit TtsLeftMenu(QWidget *parent = 0);
    ~TtsLeftMenu();

    void addItem(TtsLeftMenuItem *item);
    TtsLeftMenuItem *addItem(const QString title, const QIcon &icon);

    void activateMenuItem(TtsLeftMenuItem *item);
    TtsLeftMenuItem *activeMenuItem() const;

    void setActiveMenuItemId(int id);
    void setStackedWidget(QStackedWidget *widget);

private:
    QStackedWidget *m_stackedWidget = 0;
    QBoxLayout *layout;
    QVector<TtsLeftMenuItem *> *m_items;
    TtsLeftMenuItem *m_activeMenuItem;

signals:
    void selectedItemChanged(int id);

public slots:
    void menuItemSelected(TtsLeftMenuItem *item);

    // QWidget interface
protected:
    void resizeEvent(QResizeEvent *event);

    // QWidget interface
protected:
    void paintEvent(QPaintEvent *event);
};

#endif // TTSLEFTMENU_H
