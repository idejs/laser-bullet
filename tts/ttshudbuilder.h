#ifndef TTSHUDBUILDER_H
#define TTSHUDBUILDER_H

#include <QGraphicsPixmapItem>
#include <QGraphicsProxyWidget>
#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QObject>
#include <QSlider>

class TtsHudBuilder : public QObject
{
    Q_OBJECT
public:
    explicit TtsHudBuilder(QGraphicsScene *scene, QObject *parent = 0);

    void build(qreal offset = 25, qreal corners = 3, QPen pen = QPen(Qt::white, 1, Qt::SolidLine));

    void setNameValue(QString name);
    void setTimeValue(qint64 time);
    void setPointsValue(qint32 points);
    void setShotsValue(quint32 shots);

    void setNameVisisble(bool visible);
    void setTimeVisible(bool visible);
    void setPointsVisible(bool visible);
    void setFrameVisible(bool visible);
    void setShotsVisisble(bool visible);

    void show();
    void hide();

    void setSliderVisible(bool visible);
    bool sliderVisible() const;


    QSlider *slider() const;

protected:
    void buildFrame(qreal offset = 25, qreal corner = 3, QPen pen = QPen(Qt::white, 1, Qt::SolidLine));
    void updateVisibility();

private:
    QGraphicsScene *scene;
    QVector<QGraphicsLineItem*> frameItems;

    QGraphicsSimpleTextItem *nameItem;
    QGraphicsTextItem *timeItem;
    QGraphicsTextItem *pointsItem;
    QGraphicsSimpleTextItem *shotsItem;
    QSlider *m_slider;
    QGraphicsProxyWidget *sliderWidget;

    qreal offset;

    bool m_visible = true;
    bool m_nameVisible = true;
    bool m_frameVisible = true;
    bool m_timeVisible = false;
    bool m_pointsVisible = false;
    bool m_shotsVisible = false;

signals:
public slots:
};

#endif // TTSHUDBUILDER_H
