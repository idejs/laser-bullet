#ifndef TTSUTILS_H
#define TTSUTILS_H

#include <QImage>
#include <opencv2/opencv.hpp>

class TtsUtils
{
private:
    TtsUtils();

public:
    static QImage cvToImage(cv::Mat &source, int cvtFormat, QImage::Format imgFormat);
    static bool cvToFile(cv::Mat source, int cvtFormat, QImage::Format imgFormat, QString filename);
};

#endif // TTSUTILS_H
