#include "ttsleftmenuitem.h"

#include "ttsdebug.h"
#include "ttsleftmenu.h"

#include <QIcon>
#include <QMouseEvent>
#include <QStyleOption>

TtsLeftMenuItem::TtsLeftMenuItem(QWidget *parent) : QWidget(parent)
{

}

TtsLeftMenuItem::TtsLeftMenuItem(const QString title, const QIcon &icon, QWidget *parent) :
    QWidget(parent)
{
    m_iconSize = QSize(24, 24);
    m_image.setPixmap(icon.pixmap(m_iconSize));
    m_image.setAlignment(Qt::AlignCenter);
    m_image.resize(m_iconSize);

    m_title.setText(title);
    m_title.setFont(QFont("Droid Sans Mono", 8));
    m_title.setAlignment(Qt::AlignCenter);

    layout = new QVBoxLayout(this);
    layout->addWidget(&m_image);
    layout->addWidget(&m_title);

    setLayout(layout);

//    setGeometry(0, 0, 80, parent->rect().width());
    setFixedWidth(parentWidget()->rect().width());

    setStyleSheet("TtsLeftMenuItem:hover {background-color: #555;}");
}

void TtsLeftMenuItem::setTitle(const QString title)
{
    m_title.setText(title);
}

void TtsLeftMenuItem::setIcon(const QIcon &icon)
{
    m_image.setPixmap(icon.pixmap(m_iconSize));
}

void TtsLeftMenuItem::setIconSize(const QSize &size)
{
    m_iconSize = size;
}

void TtsLeftMenuItem::setMenu(TtsLeftMenu *menu)
{
    m_menu = menu;
}

TtsLeftMenu *TtsLeftMenuItem::menu() const
{
    return m_menu;
}

void TtsLeftMenuItem::setActiveItem(bool active)
{
    if (m_active == active)
        return;

    m_active = active;

    if (m_active)
        setStyleSheet("TtsLeftMenuItem { background-color: #1d1e1e; border: 0px; }");
    else
        setStyleSheet("TtsLeftMenuItem { border: 0px; } TtsLeftMenuItem:hover {background-color: #555;}");
}

bool TtsLeftMenuItem::active() const
{
    return m_active;
}

void TtsLeftMenuItem::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);

    adjustSize();
    m_image.setGeometry(0, 5, rect().width(), m_iconSize.height());
    m_title.setGeometry(0, m_iconSize.height() + 10, rect().width(), m_title.height());

//    m_title.move(0, m_image.rect().height() + 10);
//    m_title.resize(rect().width(), m_title.height());
}

void TtsLeftMenuItem::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);

    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

void TtsLeftMenuItem::mousePressEvent(QMouseEvent *event)
{
    TtsDebug::debug("TtsLeftMenuItem::mousePressEvent");

    if (Qt::LeftButton == event->button()) {
        TtsDebug::info("Mouse left button pressed");

        emit menuItemSelected(this);
    }
}
