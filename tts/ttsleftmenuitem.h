#ifndef TTSLEFTMENUITEM_H
#define TTSLEFTMENUITEM_H

#include <QObject>
#include <QBoxLayout>
#include <QWidget>
#include <QGraphicsScene>
#include <QLabel>

class TtsLeftMenu;

class TtsLeftMenuItem : public QWidget
{
    Q_OBJECT
public:
    explicit TtsLeftMenuItem(QWidget *parent = 0);
    TtsLeftMenuItem(const QString title, const QIcon &icon, QWidget *parent = 0);

    void setTitle(const QString title);
    void setIcon(const QIcon &icon);
    void setIconSize(const QSize &size);

    void setMenu(TtsLeftMenu *menu);
    TtsLeftMenu *menu() const;

    void setActiveItem(bool active);
    bool active() const;

private:
    TtsLeftMenu *m_menu;
    QVBoxLayout *layout;
    QSize m_iconSize;
    QGraphicsScene *m_scene;
    QLabel m_image;
    QLabel m_title;
    bool m_active = false;

    // QWidget interface
protected:
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);

signals:
    void menuItemSelected(TtsLeftMenuItem *item);
};


#endif // TTSLEFTMENUITEM_H
