#include "tts/ttscamerasettings.h"
#include "db/querybuilder.h"
#include "ttstarget.h"

#include <QVector>

#include <tts/ttsdebug.h>
#include <tts/ttscommon.h>

const QString TtsCameraSettings::ID = "id";
const QString TtsCameraSettings::NAME = "name";
const QString TtsCameraSettings::SOURCE = "source";
const QString TtsCameraSettings::ENABLED = "enabled";
const QString TtsCameraSettings::TARGET = "target";
const QString TtsCameraSettings::USER = "user";

TtsCameraSettings::TtsCameraSettings(int id, QObject *parent) : ActiveRecord(parent)
{
    if (id < 1)
        return;

    loadEntityPK(id);
}

TtsCameraSettings::TtsCameraSettings(QSqlQuery &query, QObject *parent) : ActiveRecord(parent)
{
    loadEntity(query);
}

TtsCameraSettings::TtsCameraSettings(QString source, QString target, bool enabled, QString name, QObject *parent) : ActiveRecord(parent)
{
    m_name = name;
    m_source = source;
    m_enabled = enabled;
    m_target = target;

    createEntity();
}

QVector<TtsCameraSettings *> TtsCameraSettings::all(int max, QObject *parent)
{
    QSqlQuery query(SqlQueryBuilder(TtsCameraSettings().tableName()).select()
                    ->order(ID)
                    ->buildQuery());

    execQuery(query, true);

    QVector<TtsCameraSettings *> items;

    while (query.next())
        items << new TtsCameraSettings(query, parent);

    for (; items.count() < max;)
        items << new TtsCameraSettings("", TtsTarget::defaultTarget().id(), parent);

    return items;
}

const QString TtsCameraSettings::tableName() const
{
    return "camera_settings";
}

const QStringList TtsCameraSettings::columns() const
{
    return {ID, NAME, SOURCE, ENABLED, TARGET, NAME};
}

void TtsCameraSettings::createTable()
{
    QString sql = SqlQueryBuilder(TtsCameraSettings().tableName()).createTable()
            ->column(ID, SqlQueryBuilder::INTEGER, SqlQueryBuilder::PRIMARY_KEY, true)
            ->column(NAME, SqlQueryBuilder::TEXT)
            ->column(SOURCE, SqlQueryBuilder::TEXT)
            ->column(ENABLED, SqlQueryBuilder::INTEGER)
            ->column(TARGET, SqlQueryBuilder::TEXT)
            ->column(USER, SqlQueryBuilder::INTEGER, 0)
            ->build();

    execQuery(sql, true);
}

bool TtsCameraSettings::loadEntity(const QSqlQuery &query)
{
    m_id = query.value(ID).toInt();
    m_name = query.value(NAME).toString();
    m_source = query.value(SOURCE).toString();
    m_enabled = query.value(ENABLED).toBool();
    m_target = query.value(TARGET).toString();
    m_userId = query.value(USER).toInt();

    return true;
}

bool TtsCameraSettings::createEntity()
{
    QSqlQuery query = SqlQueryBuilder(tableName()).insert()
            ->value(NAME, m_name)
            ->value(SOURCE, m_source)
            ->value(ENABLED, m_enabled)
            ->value(TARGET, m_target)
            ->value(USER, m_userId)
            ->buildQuery();

    if (!execQuery(query, true)) {
        Q_ASSERT(false);
        return false;
    }

    m_id = query.lastInsertId().toInt();

    if (!m_name.size())
        updateEntity();

    return true;
}

bool TtsCameraSettings::updateEntity()
{
    if (!m_name.size())
        m_name = QString(TTS_CAM_NAME_TEMPLATE).arg(m_id);

    QSqlQuery query = SqlQueryBuilder(tableName()).update()
            ->value(NAME, m_name)
            ->value(SOURCE, m_source)
            ->value(ENABLED, m_enabled)
            ->value(TARGET, m_target)
            ->value(USER, m_userId)
            ->where(ID + " = " + QString::number(m_id))
            ->buildQuery();

    return execQuery(query, true);
}

int TtsCameraSettings::userId() const
{
    return m_userId;
}

void TtsCameraSettings::setUserId(int userId)
{
    TtsDebug::debug(QString("TtsCameraSettings::setUserId(%1)").arg(userId));
    m_userId = userId;
    emit changed();
}

QString TtsCameraSettings::target() const
{
    return m_target;
}

void TtsCameraSettings::setTarget(const QString &target)
{
    m_target = target;
    emit changed();
}

bool TtsCameraSettings::enabled() const
{
    return m_enabled;
}

void TtsCameraSettings::setEnabled(bool enabled)
{
    m_enabled = enabled;
    emit changed();
}

QString TtsCameraSettings::source() const
{
    return m_source;
}

void TtsCameraSettings::setSource(const QString &source)
{
    m_source = source;
    emit changed();
}

QString TtsCameraSettings::name() const
{
    return m_name;
}

void TtsCameraSettings::setName(const QString &name)
{
    m_name = name;
    emit changed();
}

int TtsCameraSettings::id() const
{
    return m_id;
}
