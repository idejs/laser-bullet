﻿#include "ttsdebug.h"
#include "ttsmultishotframeprocessor.h"
#include "ttsutils.h"
#include <QDateTime>
#include <QImage>
#include <QtDebug>
#include <QtMath>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

TtsMultiShotFrameProcessor::TtsMultiShotFrameProcessor(int padding, QObject *parent) : TtsFrameProcessor(parent)
{
    m_padding = padding;

    bufferSize = sizeof(uint) * 640 * 480;
    lables = (uint *) malloc(bufferSize);
    history = (uint *) malloc(bufferSize);
    memset(history, 0, sizeof(bufferSize));

    baseHlsMat = cv::Mat(480, 640, CV_8UC3);
    lightening = cv::Mat(480, 640, CV_8UC1);
    resultHls = baseHlsMat.clone();
}

TtsMultiShotFrameProcessor::~TtsMultiShotFrameProcessor()
{
    free(lables);
    free(history);
}

void TtsMultiShotFrameProcessor::setMultiShotDetectionEnabled(bool enabled)
{
    QMutexLocker locker(&mutex);

    m_multiShotDetection = enabled;
}

void TtsMultiShotFrameProcessor::setEnabled(bool enable)
{
    QMutexLocker locker(&mutex);

    m_enabled = enable;
}

void TtsMultiShotFrameProcessor::setTrackPointsEnabled(bool enable)
{
    QMutexLocker locker(&mutex);

    m_trackPoints = enable;
}

void TtsMultiShotFrameProcessor::calibrate(int timeout)
{
    QMutexLocker locker(&mutex);

    if (m_isCalibrating)
        return;

    m_enabled = true;
    m_isCalibrating = true;
    timeout = MAX(1, timeout);

    m_calibratingTimeout = QDateTime::currentDateTime().toMSecsSinceEpoch() + timeout * 1000;

    QMetaObject::invokeMethod(this, "calibratePrivate", Qt::QueuedConnection);
}

void TtsMultiShotFrameProcessor::setCalibrationModeEnabled(bool calibrationModeEnabled)
{
    m_calibrationModeEnabled = calibrationModeEnabled;
}

uchar TtsMultiShotFrameProcessor::getLigteningLevel() const
{
    return m_ligteningLevel;
}

void TtsMultiShotFrameProcessor::calibratePrivate()
{
    baseHlsMat = cv::Scalar(0, 0, 0);
}

void TtsMultiShotFrameProcessor::calibrationFinished(bool status)
{
    if (m_isCalibrating) {
        resultHls = baseHlsMat.clone();
        m_isCalibrating = false;
        emit filterCalibrated(status);
    }
}

/**
 * @brief TtsMultiShotFrameProcessor::doCalibrate
 *
 * invoked from processFrame
 */
void TtsMultiShotFrameProcessor::doCalibrate()
{
    int col, row;
    bool calibratingActive = QDateTime::currentDateTime().toMSecsSinceEpoch() < m_calibratingTimeout;

//    for (row = m_padding; row < currentHlsMat.rows - m_padding; row++) {
//        for (col = m_padding; col < currentHlsMat.cols - m_padding; col++) {
    for (row = 0; row < currentHlsMat.rows; row++) {
        for (col = 0; col < currentHlsMat.cols; col++) {

            cv::Vec3b curItem = currentHlsMat.at<cv::Vec3b>(row, col);

            if (calibratingActive) {
//                cv::Vec3b bgItem = baseHlsMat.at<cv::Vec3b>(row, col);

//                if (curItem[1] > bgItem[1] && curItem[2] > bgItem[2])
//                    baseHlsMat.at<cv::Vec3b>(row, col) = curItem;

                if (curItem[1] > m_minLightLevel)
                    m_minLightLevel = curItem[1];

//                if (curItem[1] > lightening.at<uchar>(row, col))
//                    lightening.at<uchar>(row, col) = curItem[1];

            }

            currentFrame.at<cv::Vec3b>(row, col) = curItem[1] > m_ligteningLevel ? cv::Vec3b(255,255,255) : cv::Vec3b(0, 0, 0);
        }
    }

    if (!calibratingActive) {

        calibrationFinished(false);

//        QImage image = TtsUtils::cvToImage(baseHlsMat, CV_HLS2RGB, QImage::Format_RGB888);
//        image.save(QString("tts/calibration-%1.jpg").arg(QDateTime::currentDateTime().toMSecsSinceEpoch()));

//        image = TtsUtils::cvToImage(lightening, CV_GRAY2RGB, QImage::Format_RGB888);
//        image.save(QString("tts/lightening-%1.jpg").arg(QDateTime::currentDateTime().toMSecsSinceEpoch()));
    } else if (m_minLightLevel > m_ligteningLevel) {

        m_ligteningLevel = m_minLightLevel + (int)((255 - m_minLightLevel) * 1./3);
        emit lighteningLevelChanged(m_ligteningLevel);
    }
}

void TtsMultiShotFrameProcessor::processFrame(cv::Mat frame)
{
    QMutexLocker locker(&mutex);

    if (!m_enabled)
        return;

    currentFrame = frame;

    frameTime = QDateTime::currentDateTime().toMSecsSinceEpoch();

    memset(lables, 0, sizeof(uint) * 640 * 480);

    cv::cvtColor(frame.clone(), currentHlsMat, CV_BGR2HLS);

    if (m_isCalibrating || m_calibrationModeEnabled) {
        doCalibrate();
        return;
    }

    if (nextDetectionTime > frameTime)
        return;

    nextDetectionTime = frameTime + m_minInterval;

    // new detection algo
//    calcLightening(currentHlsMat);
//    TtsDebug::debug(QString("Frame max lightening %1 in %1 pixels").arg(m_maxFrameLightening).arg(m_maxLighteningPixels));

    QVector<QRect> rects = findPoints(m_multiShotDetection ? 16 : 1);

    if (m_trackPoints) {
        for (int i = 0; i < rects.size(); i++) {
            QRect rect = rects[i];
            cv::rectangle(frame,
                          cv::Point(rect.left()-4, rect.top()-4),
                          cv::Point(rect.right()+4, rect.bottom()+4),
                          cv::Scalar(0, 0, 255));

        }
    }

    foreach (QRect rect, rects) {
        emit rayDetected(rect);
    }
}

bool TtsMultiShotFrameProcessor::checkLightness(uchar base, uchar current)
{
    return (base > 200 && current - base > 10) ||
            (base > 100 && current - base > 100) ||
            (base > 10 && current > 150);
}

/**
 * @brief TtsMultiShotFrameProcessor::checkPixel invoked from processFrame
 * @param col
 * @param row
 * @return
 */
bool TtsMultiShotFrameProcessor::checkPixel(int col, int row)
{
    if (lables[row * 480 + col] == 1)
        return false;

    lables[row * 480 + col] = 1;

    cv::Vec3b currentPixel =  currentHlsMat.at<cv::Vec3b>(row, col);
//    cv::Vec3b basePixel = baseHlsMat.at<cv::Vec3b>(row, col);

    if (currentPixel[1] > m_ligteningLevel /*&& m_maxFrameLightening > 220 && currentPixel[1] > basePixel[1] + 30*/) {
//        TtsDebug::debug(QString("Level: %1 Current: %1").arg(ligteningLevel).arg(currentPixel[1]));
        resultHls.at<cv::Vec3b>(row, col) = currentPixel;
        return true;
    }

//    if (m_maxFrameLightening < 200)
//        return false;

//    if (currentPixel[1] < m_maxFrameLightening * 0.85)
//        return false;

//    if (!checkLightness(basePixel[1], currentPixel[1]))
//        return false;

    return false;
}

void TtsMultiShotFrameProcessor::calcLightening(const cv::Mat &mat)
{
    int row, col;
    uchar value;
    m_maxFrameLightening = 0;
    m_maxLighteningPixels = 0;

    for (row = m_padding; row < mat.rows - m_padding; row++) {
        for (col = m_padding; col < mat.cols - m_padding; col++) {
            value = mat.at<cv::Vec3b>(row, col)[1];
            if (value > m_maxFrameLightening) {
                m_maxLighteningPixels = 1;
                m_maxFrameLightening = value;
            } else if (m_maxFrameLightening == value)
                m_maxLighteningPixels++;
        }
    }
}

QRect TtsMultiShotFrameProcessor::findBoundingBox(int col, int row)
{
    QRect rect(col, row, 0, 0);
    int ir, il;

    while (true) {
        ir = il = 1;

        // find right pixel;
        while (true) {
            int pos = row * 480 + col + ir;

            if (pos > 640 * 480 || !checkPixel(col + ir, row))
                break;

            rect.setRight(col + ir);
            ir++;
        }

        // find left pixel;
        while (true) {
            int pos = row * 480 + col - il;

            if (pos < 0 || !checkPixel(col - il, row))
                break;

            rect.setLeft(col - il);
            il++;
        }

        rect.setBottom(row - 1);

        if (ir == 1 && il == 1)
            break;

        row++;
    }

    return rect;
}

/**
 * @brief TtsMultiShotFrameProcessor::findPoints invoked from processFrame
 * @param maxPoints
 * @return
 *
 */

QVector<QRect> TtsMultiShotFrameProcessor::findPoints(int maxPoints)
{
    QVector<QRect> rects;
    int col, row;

    int rows = currentHlsMat.rows - m_padding;
    int cols = currentHlsMat.cols - m_padding;

    for (row = m_padding; row < rows; row++) {
        for (col = m_padding; col < cols; col++) {

            if (!checkPixel(col, row))
                continue;

            QRect rect = findBoundingBox(col, row);
            if (rect.width() > 2 && rect.width() < 32 && rect.height() > 2 && rect.height() < 32) {
                rects.append(rect);

                if (maxPoints && rects.size() == maxPoints)
                    return rects;
            }
        }
    }

    return rects;
}

void TtsMultiShotFrameProcessor::saveResult()
{
    cv::Mat rgb;
    cv::cvtColor(resultHls, rgb, CV_HLS2RGB);

    QString fileName = QString("tts/result-%1.jpg").arg(QDateTime::currentDateTime().toMSecsSinceEpoch());

    QImage image = QImage((uchar*) rgb.data, rgb.cols, rgb.rows, rgb.step1(), QImage::Format_RGB888);
    image.save(fileName);
}

void TtsMultiShotFrameProcessor::setLaigheningLevel(int level)
{
    m_ligteningLevel = MIN(255, MAX(0, level));
}
