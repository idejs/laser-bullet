#ifndef TTSSETTINGS_H
#define TTSSETTINGS_H

#include <QObject>

class TtsSettings : public QObject
{
    Q_OBJECT
public:
    explicit TtsSettings(QObject *parent = 0);
    virtual ~TtsSettings();

    void load();
    void save();

    int cameraCount() const;
    void setCameraCount(int cameraCount);

private:
    int m_cameraCount;

signals:

public slots:
};

#endif // TTSSETTINGS_H
