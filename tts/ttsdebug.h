#ifndef TTSDEBUG_H
#define TTSDEBUG_H

#include <QObject>
#include <QString>

class TtsDebug : public QObject
{
    Q_OBJECT
private:
    explicit TtsDebug(QObject *parent = 0);
    static void message(QString &msg, QString level);
    static QString levelToStr(int level);

public:
    enum LEVEL {
        DEBUG,
        INFO,
        WARNING,
        ERROR
    };

    static void debug(QString msg);
    static void info(QString msg);
    static void error(QString msg);
    static void warning(QString msg);

signals:

public slots:
};

#endif // TTSDEBUG_H
