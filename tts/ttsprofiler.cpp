#include "ttsdebug.h"
#include "ttsprofiler.h"

#include <QDateTime>
#include <QtGlobal>

bool TtsProfiler::enabled = false;
QMap<QString, qint64> TtsProfiler::dataMap = QMap<QString, qint64>();
QMap<QString, qint64> TtsProfiler::passesMap = QMap<QString, qint64>();
QMap<QString, qint64> TtsProfiler::activeMap = QMap<QString, qint64>();

void TtsProfiler::setEnabled(bool enabled)
{
    TtsProfiler::enabled = enabled;

    if (!enabled) {
        dataMap.clear();
        passesMap.clear();
        activeMap.clear();
    }
}

void TtsProfiler::start(const QString &token)
{
    if (!enabled)
        return;

    TtsDebug::debug(QString("Start profiler: %1").arg(token));
    if (!activeMap.contains(token))
        activeMap.insert(token, QDateTime::currentMSecsSinceEpoch());
}

qint64 TtsProfiler::stop(const QString &token)
{
    if (!enabled)
        return 0;

    TtsDebug::debug(QString("Stop profiler: %1").arg(token));
    qint64 interval = activeMap.value(token, 0);
    activeMap.remove(token);
    passesMap[token]++;
    qint64 value = QDateTime::currentMSecsSinceEpoch() - interval;
    dataMap[token] += value;
    return value;
}

qint64 TtsProfiler::passes(const QString &token)
{
    return passesMap.value(token, 0);
}

qint64 TtsProfiler::ticks(const QString &token)
{
    return dataMap.value(token, 0);
}

qint64 TtsProfiler::average(const QString &token)
{
    if (passes(token) == 0)
        return 0;

    return ticks(token) / passes(token);
}

QString TtsProfiler::result(QString &token)
{
    return QString("%1: Passes %2, Ticks %3, Average: %4")
            .arg(token)
            .arg(passes(token))
            .arg(ticks(token))
            .arg(average(token));
}

void TtsProfiler::printAll()
{
    if (!enabled)
        return;

    QStringList keys = static_cast<QStringList>(dataMap.keys());
    for (int i = 0; i < keys.length(); i++) {
        TtsDebug::info(result(keys[i]));
    }
}
