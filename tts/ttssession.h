#ifndef TTSSESSION_H
#define TTSSESSION_H

class TtsCamera;
class TtsCameraSet;
class CameraViewWidget;

#include <QObject>
#include <db/ttstraining.h>
#include "ttscameraset.h"
#include "widgets/cameragrid.h"

class TtsSession : public QObject
{
    Q_OBJECT
public:
    explicit TtsSession(TtsCameraSet *set, QObject *parent = 0);

    enum TtsState {
        TTS_STATE_PREVIEW,
        TTS_STATE_PREPARE,
        TTS_STATE_PREPARED,
        TTS_STATE_STARTED,
        TTS_STATE_RESULT
    };

    bool prepare(const QString &name, int timeout);
    bool start();
    bool stop();
    bool reset();

    void setTimeout(int value);

    bool active() const;
    qint64 startTime() const;
    QString name() const;
    int state() const;
    int timeout() const;
    TtsCameraSet *cameraSet() const;
    TtsTraining *training() const;

signals:
    void stateChanged(int state);

public slots:

protected:
    void setState(const int &value);

private:
    TtsTraining *m_training;
    TtsCameraSet *m_cameraSet;
    QString m_name;
    qint64 m_startTime;
    int m_state;
    int m_timeout;
    int m_timeoutTimerId;

    // QObject interface
protected:
    void timerEvent(QTimerEvent *event);
};

#endif // TTSSESSION_H
