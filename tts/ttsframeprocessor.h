#ifndef TTSFRAMEPROCESSOR_H
#define TTSFRAMEPROCESSOR_H

#include <QObject>
#include <opencv2/opencv.hpp>

class TtsFrameProcessor : public QObject
{
public:
    TtsFrameProcessor(QObject *parent = 0);

    virtual void processFrame(cv::Mat frame) = 0;
};

#endif // TTSFRAMEPROCESSOR_H
