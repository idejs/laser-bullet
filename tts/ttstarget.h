#ifndef TTSTARGETMANAGER_H
#define TTSTARGETMANAGER_H

#include <QStringList>

class TtsTarget
{
public:
    TtsTarget(const QString &id);

    static QString path;
    static QList<TtsTarget> list();
    static TtsTarget getTarget(const QString &id);
    static TtsTarget findTarget(const QString &name);
    static TtsTarget defaultTarget();

    QString id() const;
    QString name() const;
    QString targetPath() const;
    QString maskPath() const;

private:
    static QList<TtsTarget> targetList;

    QString m_id;
    QString m_name;
    QString m_targetPath;
    QString m_maskPath;
};

#endif // TTSTARGETMANAGER_H
