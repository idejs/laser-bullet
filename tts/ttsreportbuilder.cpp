#include "ttsreportbuilder.h"
#include "ttstarget.h"

#include "tts/ttscommon.h"

#include <QDateTime>
#include <QPainter>
#include <QPrintDialog>
#include <QPrintPreviewDialog>

TtsReportBuilder::TtsReportBuilder(TtsTraining *training, QObject *parent) :
    QObject(parent),
    m_training(training)
{

}

void TtsReportBuilder::print()
{
    QPrinter printer;
    QPrintDialog dialog(&printer);
    dialog.setWindowTitle(tr("Print Training Result"));

    if (dialog.exec() != QDialog::Accepted)
        return;

    paintResults(&printer);
}

void TtsReportBuilder::printPreview()
{
    QPrintPreviewDialog dlg;
    connect(&dlg, &QPrintPreviewDialog::paintRequested, this, &TtsReportBuilder::paintResults);
    dlg.exec();
}

void TtsReportBuilder::paintResults(QPrinter *printer)
{
    if (!printer)
        return;

    QPainter painter;
    painter.begin(printer);

    int padding = 10;
    QString header = QString("№%1 от %2")
            .arg(m_training->id())
            .arg(m_training->datetime().toString(TTS_FULL_DATE_FORMAT));

    painter.setPen(QPen(Qt::black, 64, Qt::SolidLine));
    painter.setFont(QFont("Arial", 22));
    painter.drawText(QRectF(padding, 300, printer->width() - padding, 322), Qt::AlignCenter, "РЕЗУЛЬТАТЫ ПРОВЕДЕННИЯ");
    painter.drawText(QRectF(padding, 330, printer->width() - padding, 340), Qt::AlignCenter, "ТАКТИКО-ТРЕННИРОВОЧНЫХ УПРАЖНЕНИЙ");
    painter.setFont(QFont("Arial", 16));
    painter.drawText(QRectF(padding, 365, printer->width() - padding, 375), Qt::AlignCenter, header);


    QVector<TtsRoom *> rooms = TtsRoom::all(m_training, this);

    QVectorIterator<TtsRoom *> it(rooms);
    while (it.hasNext()) {
        TtsRoom *room = it.next();
        if (!room->userId())
            continue;

        printer->newPage();

        painter.setPen(QPen(Qt::black));
        painter.setFont(QFont("Arial", 16));
        painter.drawText(QRectF(padding, 0, printer->width() - padding, 16), Qt::AlignCenter, header);

        paintRoomResult(room, painter, printer);
    }

    painter.end();
}

void TtsReportBuilder::paintRoomResult(const TtsRoom *room, QPainter &painter, QPrinter *printer)
{
    Q_ASSERT(room);

    TtsUser user(room->userId());
    const QVector<TtsShot *> shots = TtsShot::fromRoom(room->id());

    QImage targetImage(TtsTarget::getTarget(room->targetId()).targetPath());
    QImage shotImage(":/graphics/shots/shot-red.png");

    QPainter shotPainter(&targetImage);

    for (int i = 0; i < shots.size(); i++) {
        TtsShot *shot = shots.at(i);

        int x = shot->x() - (shotImage.width() / 2);
        int y = shot->y() - (shotImage.height() / 2);
        shotPainter.drawImage(x, y, shotImage);
    }

    shotPainter.end();

    painter.drawImage((painter.window().width() - targetImage.width()) / 2, 0, targetImage);
    painter.setFont(QFont("Arial", 12));
    painter.drawText(QRectF(0, 40, painter.window().width(), 50), Qt::AlignLeft | Qt::AlignVCenter, user.name());
    painter.drawText(QRectF(0, 40, painter.window().width(), 50), Qt::AlignRight | Qt::AlignVCenter, QString("Результат: %1/%2").arg(room->shotCount()).arg(room->points()));

    painter.setPen(QPen(Qt::black, 1, Qt::SolidLine));
    painter.drawLine(10, 500, painter.window().width()-10, 500);

    int pixelPerLine = 20;
    int tableTop = 540;
    int rowsPerColum = (painter.window().height() - tableTop) / pixelPerLine;

    painter.setFont(QFont("Courier New", 10));
    painter.setPen(QPen(Qt::black, 1, Qt::SolidLine));
    for (int i = 0; i < shots.size(); i++) {
        int row = i / rowsPerColum;
        int column = i % rowsPerColum;

        if (i != 0 && row == 3 && column == 0) {
            printer->newPage();
            painter.setPen(QPen(Qt::black, 1, Qt::SolidLine));
            painter.setFont(QFont("Arial", 12));
            painter.drawText(QRectF(0, 0, painter.window().width(), 20), Qt::AlignLeft | Qt::AlignVCenter, user.name());
            painter.drawText(QRectF(0, 0, painter.window().width(), 20), Qt::AlignRight | Qt::AlignVCenter, QString("Результат: %1/%2").arg(room->shotCount()).arg(room->points()));

            tableTop = 100;
            rowsPerColum = (painter.window().height() - tableTop) / pixelPerLine;
        }

        if (row > 2) row %= 3;


        QString shotTime = QDateTime::fromMSecsSinceEpoch(shots[i]->time()).toString("mm:ss:zz");


        painter.setPen(QPen(QColor::fromHsv(0, 255/10*shots[i]->points(), 128), 1, Qt::SolidLine));
        painter.drawText(row * painter.window().width() / 3, tableTop + column * 20, QString("%1. %2 >> %3")
                         .arg(i, 3)
                         .arg(shotTime, -12, QLatin1Char('0'))
                         .arg(shots[i]->points(), 3));

    }
}
