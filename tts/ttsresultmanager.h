#ifndef TTSRESULTMANAGER_H
#define TTSRESULTMANAGER_H

#include "ttssession.h"

#include <QObject>
#include <QVector>
#include <QXmlStreamWriter>

class TtsResultManager : public QObject
{
    Q_OBJECT
public:
    explicit TtsResultManager(QObject *parent = 0);

    void exportSession(TtsSession *session, QString filename);

private:
    void writeCameraInfo(QXmlStreamWriter &writer, TtsCamera *camera);
//    void writeHitsInfo(QXmlStreamWriter &writer, TtsHitCollection *hits);

signals:

public slots:
};

#endif // TTSRESULTMANAGER_H
