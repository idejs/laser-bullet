#ifndef TTSMULTISHOTFRAMEPROCESSOR_H
#define TTSMULTISHOTFRAMEPROCESSOR_H

#include "ttsframeprocessor.h"

#include <QColor>
#include <QMutex>
#include <QRect>
#include <QVector>

class TtsMultiShotFrameProcessor : public TtsFrameProcessor
{
    Q_OBJECT
public:
    TtsMultiShotFrameProcessor(int borders = 25, QObject *parent = 0);
    ~TtsMultiShotFrameProcessor();

    void setMultiShotDetectionEnabled(bool enabled);
    void setEnabled(bool enable);
    void setTrackPointsEnabled(bool enable);
    void calibrate(int timeout = 3);
    uchar getLigteningLevel() const;
    void setCalibrationModeEnabled(bool calibrationModeEnabled);


private:
    QMutex mutex;

    volatile bool m_enabled = false;
    volatile bool m_trackPoints = false;
    volatile int m_padding = 5;
    volatile bool m_isCalibrating = false;
    volatile qint64 m_calibratingTimeout;
    volatile bool m_multiShotDetection = false;
    volatile uchar m_ligteningLevel = 0;
    volatile bool m_calibrationModeEnabled = false;

    int m_minInterval = 50;
    uchar m_minLightLevel = 0;
    uchar m_maxFrameLightening;
    uchar m_maxLighteningPixels;

    uint step;
    uint *lables;
    uint *history;
    uint bufferSize = 0;

    cv::Mat currentFrame;
    cv::Mat currentHlsMat;
    cv::Mat baseHlsMat;
    cv::Mat resultHls;
    cv::Mat lightening;

    qint64 nextDetectionTime = 0;
    qint64 frameTime;

    Q_INVOKABLE void calibratePrivate();
    void doCalibrate();
    void calibrationFinished(bool status = true);

    // TtsFrameProcessor interface
public:
    void processFrame(cv::Mat currentHlsMat);

protected:
    bool checkLightness(uchar base, uchar current);
    bool checkPixel(int col, int row);
    void calcLightening(const cv::Mat &mat);
    QRect findBoundingBox(int col, int row);
    QVector<QRect> findPoints(int maxPoints = 16);

public slots:
    void saveResult();
    void setLaigheningLevel(int level);

signals:
    void rayDetected(const QRect point);
    void multiRayDetected(const QVector<QRect> points);
    void filterCalibrated(bool adjusted);
    void lighteningLevelChanged(int value);
};

#endif // TTSMULTISHOTFRAMEPROCESSOR_H
