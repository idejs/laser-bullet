#include "ttsdebug.h"
#include "ttsthreadedcameradevice.h"

#include <QApplication>
#include <QtDebug>
#include <opencv2/imgproc/imgproc.hpp>

TtsThreadedCameraDevice::TtsThreadedCameraDevice(int id, const QString &source, TtsMultiShotFrameProcessor *frameProcessor, QObject *parent) :
    QObject(parent),
    m_id(id)
{
    setState(TtsThreadedCameraDevice::STATE_UNCONFIGURED);
    setSource(source);

    processor = frameProcessor;
    if (processor)
        processor->setParent(this);

    captureTimer = new QTimer(this);
    captureTimer->setTimerType(Qt::PreciseTimer);

    connect(captureTimer, &QTimer::timeout, [=]() {
        captureFrame();
    });
}

bool TtsThreadedCameraDevice::setSource(const QString &source)
{
    if (m_source == source)
        return false;

    m_source = source;

    setState(m_source.length() == 0 ? STATE_UNCONFIGURED : STATE_CLOSED);

    return true;
}

bool TtsThreadedCameraDevice::open()
{
    if (!device)
        device = new cv::VideoCapture;

    if (device->isOpened())
        return true;

    setState(STATE_CONNECTING);

    if (m_source.startsWith("/dev/video")) {

        int v4l2Device = m_source.mid(10).toInt();
        if (v4l2Device >= 0 && device->open(v4l2Device)) {
            connectionEstablished();
            return true;
        }

    } else if (m_source.startsWith("http://") || m_source.startsWith("https://")) {
        if (device->open(m_source.toStdString())) {
            connectionEstablished();
            return true;
        }
    }

//    setState(STATE_ERROR);

    return false;
}

bool TtsThreadedCameraDevice::opened()
{
    return STATE_CAPTURING == m_state;
}

bool TtsThreadedCameraDevice::capturing() const
{
    return captureTimer->isActive();
}

void TtsThreadedCameraDevice::setState(int state)
{
    QMutexLocker locker(&stateMutex);

    if (m_state != state) {
        m_state = state;
        emit stateChanged(state);

        TtsDebug::info(QString("CAM%1 state changed: %2").arg(m_id).arg(textState()));
    }
}

int TtsThreadedCameraDevice::state()
{
    QMutexLocker locker(&stateMutex);

    return m_state;
}

QImage TtsThreadedCameraDevice::convertToImage(cv::Mat &frame)
{
    cv::Mat cvtFrame;
    cv::cvtColor(frame, cvtFrame, frame.channels() == 1 ? CV_GRAY2RGBA : CV_BGR2RGB);

    return QImage((uchar*) cvtFrame.data, cvtFrame.cols, cvtFrame.rows, cvtFrame.step1(), QImage::Format_RGB888);
}

void TtsThreadedCameraDevice::captureFrame()
{
    QMutexLocker locker(&mutex);

    if (STATE_UNCONFIGURED == state())
        return;


    if (STATE_CONNECTION_LOST == state() || STATE_LOST == state()) {
        if (open())
            connectionEstablished();

    } else if (!device->isOpened()) {
        connectionLost();
        return;
    }

    cv::Mat rawBgrFrame;

    if (!device->read(rawBgrFrame)) {
        TtsDebug::warning("Frame not ready");
        connectionLost();
        return;
    }

    setState(STATE_CAPTURING);

    preparedBgrFrame = prepareFrame(rawBgrFrame);

    if (processor != 0)
        processor->processFrame(preparedBgrFrame);

    QImage rgbImage = convertToImage(preparedBgrFrame);

    emit frameReady(rgbImage);

    if (m_makeScreenshot) {
        m_makeScreenshot = false;
        emit screenshotReady(rgbImage);
    }
}

cv::Mat TtsThreadedCameraDevice::prepareFrame(const cv::Mat source)
{
    cv::Mat dest;
    cv::GaussianBlur(source, dest, cv::Size(3, 3), 0, 0);
    return dest;
}

void TtsThreadedCameraDevice::changeSource(const QString source)
{
    TtsDebug::debug(QString("TtsThreadedCameraDevice::changeSource, source: %1").arg(source));

    if (m_source == source)
        return;

//    bool isCapturing = capturing();

    stop();
    setSource(source);
    capture();
}

/*!
 * @brief TtsThreadedCameraDevice::capture
 */
void TtsThreadedCameraDevice::capture()
{   
    if (STATE_UNCONFIGURED == m_state)
        return;

    if (capturing())
        return;

    open();

    if (!device->isOpened())
        setState(STATE_ERROR);

    captureTimer->start(m_captureTimeout);
    captureFrame();
}

/**
 * @brief TtsThreadedCameraDevice::stop
 */
void TtsThreadedCameraDevice::stop()
{
    if (!capturing())
        return;

    captureTimer->stop();

    setState(STATE_STOPED);

    release();
}

void TtsThreadedCameraDevice::screenshot()
{
    m_makeScreenshot = true;
}

void TtsThreadedCameraDevice::release()
{
    if (!device)
        return;

    device->release();
    delete device;
    device = 0;
}

QString TtsThreadedCameraDevice::textState(int state)
{
    if (state == -1)
        state = m_state;

    switch (state) {
    case STATE_UNCONFIGURED: return "UNCONFIGURED";
    case STATE_CONFIGURED: return "CONFIGURED";
    case STATE_CLOSED: return "CLOSED";
    case STATE_STOPED: return "STOPED";
    case STATE_CONNECTING: return "CONNECTING";
    case STATE_CONNECTION_LOST: return "CONNECTION LOST";
    case STATE_CAPTURING: return "CAPTURING";
    case STATE_ERROR: return "ERROR";
    case STATE_LOST: return "LOST";
    default: return "UNKNOWN STATE";
    }

}

void TtsThreadedCameraDevice::connectionLost()
{
    setState(STATE_CONNECTION_LOST);
    release();

    captureTimer->stop();

    if (++m_retries >= m_maxRetries) {
        setState(STATE_LOST);
        captureTimer->start(10000);
        return;
    }

    captureTimer->start(m_timeout);
}

void TtsThreadedCameraDevice::connectionEstablished()
{
    setState(STATE_CAPTURING);

    captureTimer->stop();
    captureTimer->start(m_captureTimeout);
    m_retries = 0;
}
