#ifndef TTSDB_H
#define TTSDB_H

#include <QObject>
#include <QSqlQuery>

class TtsDb : public QObject
{
    Q_OBJECT
    explicit TtsDb(QObject *parent = 0);

    static TtsDb *INSTANCE;

public:
    static TtsDb *instance();

    bool init();

    static bool execQuery(QSqlQuery &query, bool assert = false);

private:

signals:

public slots:
};

#endif // TTSDB_H
