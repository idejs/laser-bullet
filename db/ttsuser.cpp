#include "ttsuser.h"
#include "querybuilder.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>

#include "tts/ttsdebug.h"

const QString TtsUser::ID = "id";
const QString TtsUser::NAME = "name";
const QString TtsUser::TRAININGS = "trainings";
const QString TtsUser::SCORES = "scores";
const QString TtsUser::STATE = "state";

TtsUser::TtsUser(int id, QObject *parent) : ActiveRecord(parent)
{
    loadEntityPK(id);
}

TtsUser::TtsUser(QSqlQuery &query, QObject *parent) : ActiveRecord(parent)
{
    loadEntity(query);
}

TtsUser::TtsUser(const QString &name, int state, QObject *parent) : ActiveRecord(parent)
{
    m_name = name;
    m_state = state;

    createEntity();
}

QVector<TtsUser *> TtsUser::all(QObject *parent)
{
    QSqlQuery query = SqlQueryBuilder(TtsUser().tableName()).select()
            ->order(ID)
            ->buildQuery();

    execQuery(query, true);

    QVector<TtsUser *> list;
    QVector<TtsUser *> deleted;

    while(query.next()) {
        TtsUser *item = new TtsUser(query, parent);

        if (item->deleted())
            deleted << item;
        else
            list << item;
    }

    list << deleted;

    return list;
}

QVector<TtsUser *> TtsUser::all(int state, QObject *parent)
{
    QSqlQuery query = SqlQueryBuilder(TtsUser().tableName()).select()
            ->where(STATE + " = " + QString::number(state))
            ->order(ID)
            ->buildQuery();

    execQuery(query, true);

    QVector<TtsUser *> list;

    while(query.next())
        list << new TtsUser(query, parent);

    return list;
}

QMap<int, TtsUser *> TtsUser::map(QObject *parent)
{
    QMap<int, TtsUser *> items;

    foreach (TtsUser *item, TtsUser::all(parent))
        items.insert(item->id(), item);

    return items;
}

TtsUser *TtsUser::find(const QString &name, QObject *parent)
{
    QSqlQuery query = SqlQueryBuilder(TtsUser().tableName()).select()
            ->where(NAME + " = ?")
            ->buildQuery();

    query.addBindValue(name);

    if (!execQuery(query, false) || !query.next())
        return 0;

    return new TtsUser(query, parent);
}

TtsUser TtsUser::find(const QString &name)
{
    QSqlQuery query = SqlQueryBuilder(TtsUser().tableName()).select()
            ->where(NAME + " = ?")
            ->buildQuery();

    query.addBindValue(name);

    if (!execQuery(query, false) || !query.next())
        return TtsUser(-1, 0);

    return TtsUser(query);
}

QString TtsUser::name() const
{
    return m_name;
}

void TtsUser::setName(const QString &name)
{
    m_name = name;
    emit changed();
}

int TtsUser::trainings() const
{
    return m_trainings;
}

qreal TtsUser::scores() const
{
    return m_scores;
}

int TtsUser::state() const
{
    return m_state;
}

void TtsUser::setState(int state)
{
    m_state = state;
    emit changed();
}

void TtsUser::remove()
{
    setState(STATE_DELETED);
}

void TtsUser::addTrainingResult(qreal scores)
{
    m_trainings++;
    m_scores += scores;

    emit changed();
}

bool TtsUser::active() const
{
    return STATE_ACTIVE == m_state;
}

bool TtsUser::deleted() const
{
    return STATE_DELETED == m_state;
}

int TtsUser::id() const
{
    return m_id;
}

bool TtsUser::loadEntity(const QSqlQuery &query)
{
    m_id = query.value(ID).toInt();
    m_name = query.value(NAME).toString();
    m_trainings = query.value(TRAININGS).toInt();
    m_scores = query.value(SCORES).toReal();
    m_state = query.value(STATE).toInt();

    return true;
}

bool TtsUser::createEntity()
{
    QSqlQuery query = SqlQueryBuilder(tableName()).insert()
            ->value(NAME, m_name)
            ->value(TRAININGS, m_trainings)
            ->value(SCORES, m_scores)
            ->value(STATE, m_state)
            ->buildQuery();

    if (!execQuery(query, true))
        return false;

    m_id = query.lastInsertId().toInt();
    return true;
}

bool TtsUser::updateEntity()
{
    QSqlQuery query = SqlQueryBuilder(tableName()).update()
            ->value(NAME, m_name)
            ->value(TRAININGS, m_trainings)
            ->value(SCORES, m_scores)
            ->value(STATE, m_state)
            ->where(ID + " = " + QString::number(m_id))
            ->buildQuery();

    return execQuery(query, true);
}

void TtsUser::createTable()
{
    QString sql = SqlQueryBuilder(TtsUser().tableName()).createTable()
            ->column(ID, SqlQueryBuilder::INTEGER, SqlQueryBuilder::PRIMARY_KEY, true)
            ->column(NAME, SqlQueryBuilder::TEXT, SqlQueryBuilder::UNIQUE_KEY)
            ->column(TRAININGS, QString::number(0), SqlQueryBuilder::INTEGER)
            ->column(SCORES, QString::number(0), SqlQueryBuilder::REAL)
            ->column(STATE, SqlQueryBuilder::INTEGER)
            ->build();

    execQuery(sql, true);
}

const QStringList TtsUser::columns() const
{
    return {ID, NAME, TRAININGS, SCORES, STATE};
}


const QString TtsUser::tableName() const
{
    return "user";
}
