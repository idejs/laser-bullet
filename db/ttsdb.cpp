#include "ttsdb.h"

#include <QDir>
#include <QFile>
#include <QSqlQuery>
#include <QSqlError>

#include "tts/ttsdebug.h"
#include "db/ttstraining.h"
#include "db/ttsuser.h"
#include "db/ttsroom.h"

#include <tts/ttscamerasettings.h>

TtsDb *TtsDb::INSTANCE = 0;

TtsDb::TtsDb(QObject *parent) : QObject(parent)
{

}

TtsDb *TtsDb::instance()
{
    if (!INSTANCE)
        INSTANCE = new TtsDb();

    return INSTANCE;
}

bool TtsDb::init()
{
    QString dbFilename = "lbullet.dat";
    QString backupFilename = "lbullet.bak";

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbFilename);

//    QFile::remove(backupFilename);
//    QFile::remove(dbFilename);

    if (QFile::exists(dbFilename))
        TtsDebug::info("Connecting to database...");
    else
        TtsDebug::info("Creating new database...");

    if (!db.open()) {
        TtsDebug::warning("Error opening database file " + dbFilename);

        if (QFile::exists(backupFilename)) {
            TtsDebug::info("Found database backup file " + backupFilename);
            TtsDebug::info("Restore database from backup...");

            if (!QFile::remove(dbFilename)) {
                TtsDebug::error("Can`t delete database file. Access denied. Exiting.");
                return false;
            }

            if (QFile::copy(backupFilename, dbFilename)) {
                TtsDebug::info("Database backup restored. Try to open.");

                db.setDatabaseName(dbFilename);
                if (!db.open()) {
                    TtsDebug::warning("Database backup corrupt. Removing.");
                    QFile::remove(dbFilename);

                    TtsDebug::info("Creating new database " + dbFilename);
                    if (!db.open()) {
                        TtsDebug::error("Can`t create database. Exiting.");
                        return false;
                    }
                    TtsDebug::info("New database created");
                }


            } else {
                TtsDebug::error("Can`t restore database backup. ");

                TtsDebug::info("Creating new database " + dbFilename + ".");
                if (!db.open()) {
                    TtsDebug::error("Can`t create database. Exiting.");
                    return false;
                }
                TtsDebug::info("New database created.");
            }
        } else {
            TtsDebug::warning("Database backup not found. Removing database.");
            QFile::remove(dbFilename);

            TtsDebug::info("Creating new database " + dbFilename + ".");
            if (!db.open()) {
                TtsDebug::error("Can`t create database. Exiting.");
                return false;
            }
            TtsDebug::info("New database created.");
        }
    } else {
        TtsDebug::info("Database opened.");
    }

    TtsDebug::info("Making database backup.");
    QFile::copy(dbFilename, backupFilename);
    // TODO: make daily backup

    TtsDebug::info("Initializing database...");

    bool transaction = QSqlDatabase::database().transaction();

    TtsCameraSettings::createTable();
    TtsUser::createTable();
    TtsTraining::createTable();
    TtsShot::createTable();
    TtsRoom::createTable();

    if (transaction && !QSqlDatabase::database().commit())
        QSqlDatabase::database().rollback();

    TtsDebug::info("Database initialization complete.");

    return true;
}

bool TtsDb::execQuery(QSqlQuery &query, bool assert)
{
    if (!query.exec()) {
        TtsDebug::error("Database error in query `" + query.executedQuery() + "` - " + query.lastError().text());
        Q_ASSERT(assert);
        return false;
    }

    return true;
}
