#ifndef SQLQUERYBUILDER_H
#define SQLQUERYBUILDER_H

#include <QMap>
#include <QObject>
#include <QSqlQuery>
#include <QVariant>
#include <QVector>

class CreateTableSqlQueryBuilder;
class InsertSqlQueryBuilder;
class SelectSqlQueryBuilder;
class UpdateSqlQueryBuilder;
class BaseSqlQueryBuilder;

class SqlQueryBuilder : public QObject
{
    Q_OBJECT
public:
    enum {
        INTEGER,
        TEXT,
        REAL
    };

    enum {
        PRIMARY_KEY = 1,
        UNIQUE_KEY,
        KEY
    };


    explicit SqlQueryBuilder(QObject *parent = 0);
    explicit SqlQueryBuilder(const QString &table, QObject *parent = 0);

    void clear();

    CreateTableSqlQueryBuilder *createTable(bool ifNotExists = true);
    BaseSqlQueryBuilder *insert();
    BaseSqlQueryBuilder *update();
    BaseSqlQueryBuilder *select(const QString &fields = "*");

    QString type(int t);
    QString build();

    QString table() const;
    void setTable(const QString &table);

protected:
    QString m_table;

signals:

public slots:
};

/*!
 * \brief The CreateTableSqlQueryBuilder class
 *
 * Senks to friends
 */
class CreateTableSqlQueryBuilder : public QObject
{
    Q_OBJECT
public:
    explicit CreateTableSqlQueryBuilder(SqlQueryBuilder *builder, bool ifNotExists = true);

    CreateTableSqlQueryBuilder *column(const QString &name, int type = SqlQueryBuilder::TEXT, int key = -1, bool autoIncrement = false);
    CreateTableSqlQueryBuilder *column(const QString &name, const QString &defaultValue, int type = SqlQueryBuilder::TEXT, int key = -1, bool autoIncrement = false);
    QString build() const;
    QSqlQuery buildQuery() const;

private:
    SqlQueryBuilder *m_builder;
    QString m_statement;
    QStringList m_columns;
};

class BaseSqlQueryBuilder : public QObject
{
    Q_OBJECT
public:
    explicit BaseSqlQueryBuilder(SqlQueryBuilder *builder);

    BaseSqlQueryBuilder *where(const QString &clause);
    BaseSqlQueryBuilder *andWhere(const QString &clause);
    BaseSqlQueryBuilder *orWhere(const QString &clause);
    BaseSqlQueryBuilder *order(const QString &column, bool ascending = true);
    BaseSqlQueryBuilder *value(const QString &column, const QVariant &value);

    virtual QSqlQuery buildQuery() = 0;

protected:
    SqlQueryBuilder *m_builder;

    QVector<QString> m_columns;
    QVector<QVariant> m_values;

    QString m_whereClause;
    QString m_order;
};

class InsertSqlQueryBuilder : public BaseSqlQueryBuilder
{
    Q_OBJECT
public:
    explicit InsertSqlQueryBuilder(SqlQueryBuilder *builder);

    QSqlQuery buildQuery();
};

class UpdateSqlQueryBuilder : public BaseSqlQueryBuilder
{
    Q_OBJECT
public:
    explicit UpdateSqlQueryBuilder(SqlQueryBuilder *builder);

    QSqlQuery buildQuery();
};

class SelectSqlQueryBuilder : public BaseSqlQueryBuilder
{
    Q_OBJECT
public:
    explicit SelectSqlQueryBuilder(const QString &fields, SqlQueryBuilder *builder);
    explicit SelectSqlQueryBuilder(const QStringList &fields, SqlQueryBuilder *builder);

    QSqlQuery buildQuery();

protected:
    QStringList m_fields;
};



#endif // SQLQUERYBUILDER_H
