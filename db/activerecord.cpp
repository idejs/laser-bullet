#include "activerecord.h"
#include "querybuilder.h"
#include "tts/ttsdebug.h"

#include <QSqlError>
#include <QMetaObject>

ActiveRecord::ActiveRecord(QObject *parent) : QObject(parent)
{
    connect(this, &ActiveRecord::changed, this, &ActiveRecord::entityChanged);
}

bool ActiveRecord::execQuery(QSqlQuery &query, bool assert)
{
//    TtsDebug::info("Exec query: " + query.lastQuery());

    if (!query.exec()) {
//        TtsDebug::error("Database error in query `" + query.lastQuery() + "` - " + query.lastError().text());
        Q_ASSERT(!assert);
        return false;
    }

    return true;
}

QSqlQuery ActiveRecord::execQuery(const QString &sql, bool assert)
{
//    TtsDebug::info(sql);

    QSqlQuery query(sql);
    execQuery(query, assert);

    return query;
}

/*!
 * \brief TtsActiveRecord::column
 * \param column position
 * \return column name
 *
 * Exclamation of friends
 */
const QString ActiveRecord::column(int position)
{
    Q_ASSERT(position < columns().size());
    return columns().at(position);
}

const QString ActiveRecord::pk() const
{
    return "id";
}

bool ActiveRecord::loadEntityPK(int id)
{
    Q_ASSERT(pk().size());

    if (id < 1)
        return false;

    QSqlQuery query = SqlQueryBuilder(tableName()).select()->where(pk() + " = ?")->buildQuery();
    query.bindValue(0, id);

    execQuery(query, true);

    if (!query.next())
        return false;

    return loadEntity(query);
}

void ActiveRecord::entityChanged()
{
    m_changed = true;
    update();
}

bool ActiveRecord::update()
{
    if (!m_changed || !updateEntity())
        return false;

    m_changed = false;
    return true;
}

bool ActiveRecord::doUpdate()
{
    emit changed();
    return true;
}
