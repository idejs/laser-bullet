#include "ttsroom.h"
#include "querybuilder.h"

#include "QVariant"

#include <QPoint>

const QString TtsRoom::ID = "id";
const QString TtsRoom::TRAINING = "training";
const QString TtsRoom::USER = "user";
const QString TtsRoom::CAMERA = "camera";
const QString TtsRoom::TARGET = "target";

TtsRoom::TtsRoom(int id, QObject *parent) : ActiveRecord(parent)
{
    loadEntityPK(id);
}

TtsRoom::TtsRoom(QSqlQuery &query, QObject *parent)
{
    Q_UNUSED(parent);
    loadEntity(query);
}

TtsRoom::TtsRoom(const TtsTraining *training, const TtsUser *user, int camera, const QString &target)
{
    Q_ASSERT(training);

    m_trainingId = training->id();
    m_userId = user ? user->id() : 0;
    m_cameraId = camera;
    m_targetId = target;

    createEntity();
}

QVector<TtsRoom *> TtsRoom::all(QObject *parent)
{
    QSqlQuery query = SqlQueryBuilder(TtsRoom().tableName()).select()
            ->order(ID)
            ->buildQuery();

    execQuery(query, true);

    QVector<TtsRoom *> items;

    while (query.next())
        items << new TtsRoom(query, parent);

    return items;
}

QVector<TtsRoom *> TtsRoom::all(const TtsTraining *training, QObject *parent)
{
    Q_ASSERT(training);

    QSqlQuery query = SqlQueryBuilder(TtsRoom().tableName()).select()
            ->where(TRAINING + " = " + QString::number(training->id()))
            ->order(ID)
            ->buildQuery();

    execQuery(query, true);

    QVector<TtsRoom *> items;

    while (query.next())
        items << new TtsRoom(query, parent);

    return items;
}

void TtsRoom::createTable()
{
    QString sql = SqlQueryBuilder(TtsRoom().tableName()).createTable()
            ->column(ID, SqlQueryBuilder::INTEGER, SqlQueryBuilder::PRIMARY_KEY, true)
            ->column(TRAINING, SqlQueryBuilder::INTEGER, SqlQueryBuilder::KEY)
            ->column(USER, SqlQueryBuilder::INTEGER)
            ->column(CAMERA, SqlQueryBuilder::INTEGER)
            ->column(TARGET, SqlQueryBuilder::TEXT)
            ->build();

    execQuery(sql, true);
}

const QStringList TtsRoom::columns() const
{
    return {ID, TRAINING, USER, CAMERA, TARGET};
}

bool TtsRoom::loadEntity(const QSqlQuery &query)
{
    m_id = query.value(ID).toInt();
    m_trainingId = query.value(TRAINING).toInt();
    m_userId = query.value(USER).toInt();
    m_cameraId = query.value(CAMERA).toInt();
    m_targetId = query.value(TARGET).toInt();
    m_points = 0;

    m_shots = TtsShot::fromRoom(m_id);

    foreach (TtsShot *shot, m_shots)
        m_points += shot->points();

    return true;
}

bool TtsRoom::createEntity()
{
    QSqlQuery query = SqlQueryBuilder(tableName()).insert()
            ->value(TRAINING, m_trainingId)
            ->value(USER, m_userId)
            ->value(CAMERA, m_cameraId)
            ->value(TARGET, m_targetId)
            ->buildQuery();

    if (!execQuery(query, true)) {
        Q_ASSERT(false);
        return false;
    }

    m_id = query.lastInsertId().toInt();
    return true;
}

bool TtsRoom::updateEntity()
{
    QSqlQuery query = SqlQueryBuilder(tableName()).update()
            ->value(TRAINING, m_trainingId)
            ->value(USER, m_userId)
            ->value(CAMERA, m_cameraId)
            ->value(TARGET, m_targetId)
            ->where(ID + " = " + QString::number(m_id))
            ->buildQuery();

    return execQuery(query, true);
}

int TtsRoom::points() const
{
    return m_points;
}

int TtsRoom::shotCount() const
{
    return m_shots.size();
}

QString TtsRoom::targetId() const
{
    return m_targetId;
}

TtsShot *TtsRoom::addShot(int x, int y, int time, int points)
{
    m_points += points;

    TtsShot *shot = new TtsShot(id(), x, y, time, points);
    m_shots.append(shot);
    return shot;
}

TtsShot *TtsRoom::addShot(const QPoint &point, int time, int points)
{
    return addShot(point.x(), point.y(), time, points);
}

const QVector<TtsShot *> TtsRoom::shots()
{
    return m_shots;
}

int TtsRoom::cameraId() const
{
    return m_cameraId;
}

int TtsRoom::userId() const
{
    return m_userId;
}

int TtsRoom::trainingId() const
{
    return m_trainingId;
}

int TtsRoom::id() const
{
    return m_id;
}

const QString TtsRoom::tableName() const
{
    return "room";
}
