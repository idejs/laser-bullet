#ifndef TTSROOM_H
#define TTSROOM_H

#include "activerecord.h"
#include "ttsshot.h"
#include "ttstraining.h"
#include "ttsuser.h"

#include <QVector>

class TtsRoom : public ActiveRecord
{
    Q_OBJECT
public:
    static const QString ID;
    static const QString TRAINING;
    static const QString USER;
    static const QString CAMERA;
    static const QString TARGET;

    explicit TtsRoom(int id = -1, QObject *parent = 0);
    explicit TtsRoom(QSqlQuery &query, QObject *parent = 0);
    explicit TtsRoom(const TtsTraining *training, const TtsUser *user, int camera, const QString &target);

    static void createTable();
    static QVector<TtsRoom *> all(QObject *parent = 0);
    static QVector<TtsRoom *> all(const TtsTraining *training, QObject *parent = 0);

    int id() const;
    int trainingId() const;
    int userId() const;
    int cameraId() const;
    QString targetId() const;

    TtsShot *addShot(int x, int y, int time, int points);
    TtsShot *addShot(const QPoint &point, int time, int points);
    const QVector<TtsShot *> shots();
    int points() const;
    int shotCount() const;

signals:

public slots:

    // TtsActiveRecord interface
protected:
    const QStringList columns() const;
    bool loadEntity(const QSqlQuery &query);
    bool createEntity();
    bool updateEntity();

private:
    int m_id;
    int m_trainingId;
    int m_userId;
    int m_cameraId;
    QString m_targetId;

    QVector<TtsShot *> m_shots;
    int m_points = 0;


    // TtsActiveRecord interface
public:
    const QString tableName() const;
};

#endif // TTSROOM_H
