#ifndef ACTIVERECORD_H
#define ACTIVERECORD_H

#include <QObject>
#include <QSqlQuery>

class ActiveRecord : public QObject
{
    Q_OBJECT
public:
    explicit ActiveRecord(QObject *parent = 0);

    static bool execQuery(QSqlQuery &query, bool assert = false);
    static QSqlQuery execQuery(const QString &sql, bool assert = false);

    bool update();
    const QString column(int position);

    virtual const QString tableName() const = 0;
    virtual const QStringList columns() const = 0;
    virtual const QString pk() const;

protected:
    bool loadEntityPK(int id);
    bool doUpdate();

    virtual bool loadEntity(const QSqlQuery &query) = 0;
    virtual bool createEntity() = 0;
    virtual bool updateEntity() = 0;

private:
    QString m_tableName;
    bool m_changed = false;

signals:
    void changed();

public slots:

private slots:
    void entityChanged();
};

#endif // ACTIVERECORD_H
