#ifndef TTSTRAINING_H
#define TTSTRAINING_H

#include "activerecord.h"

#include <QDateTime>
#include <QVector>

class TtsTraining : public ActiveRecord
{
    Q_OBJECT
public:
    static const QString ID;
    static const QString NAME;
    static const QString TIME;
    static const QString DURATION;

    explicit TtsTraining(int id = -1, QObject *parent = 0);
    explicit TtsTraining(QSqlQuery &query, QObject *parent = 0);
    explicit TtsTraining(const QString &name, qint64 start, qint64 duration);

    static void createTable();
    static const QVector<TtsTraining *> all(QObject *parent = 0);

    int id() const;

    const QString name() const;
    TtsTraining *setName(const QString &name);

    qint64 timestamp() const;
    qint64 duration() const;
    QDateTime datetime() const;

private:
    int m_id;
    QString m_name;
    qint64 m_time;
    qint64 m_duration;

signals:

public slots:

    // TtsActiveRecord interface
protected:
    virtual bool loadEntity(const QSqlQuery &query);
    virtual bool updateEntity();
    virtual bool createEntity();

    // TtsActiveRecord interface
public:
    const QStringList columns() const;
    const QString tableName() const;
};

#endif // TTSTRAINING_H
