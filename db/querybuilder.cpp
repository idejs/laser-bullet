#include "querybuilder.h"

#include <QVector>

#include <tts/ttsdebug.h>

SqlQueryBuilder::SqlQueryBuilder(QObject *parent) : QObject(parent)
{

}

SqlQueryBuilder::SqlQueryBuilder(const QString &table, QObject *parent) :
    QObject(parent), m_table(table)
{

}

void SqlQueryBuilder::clear()
{

}

CreateTableSqlQueryBuilder *SqlQueryBuilder::createTable(bool ifNotExists)
{
    return new CreateTableSqlQueryBuilder(this, ifNotExists);
}

BaseSqlQueryBuilder *SqlQueryBuilder::insert()
{
    return new InsertSqlQueryBuilder(this);
}

BaseSqlQueryBuilder *SqlQueryBuilder::update()
{
    return new UpdateSqlQueryBuilder(this);
}

BaseSqlQueryBuilder *SqlQueryBuilder::select(const QString &fields)
{
    return new SelectSqlQueryBuilder(fields, this);
}

QString SqlQueryBuilder::table() const
{
    return m_table;
}

void SqlQueryBuilder::setTable(const QString &table)
{
    m_table = table;
}

QString SqlQueryBuilder::type(int t)
{
    switch (t) {
    case INTEGER: return "INTEGER";
    case REAL:  return "REAL";
    case TEXT: return "TEXT";
    }
    Q_ASSERT(false);
    return "UNKNOWN";
}

CreateTableSqlQueryBuilder::CreateTableSqlQueryBuilder(SqlQueryBuilder *builder, bool ifNotExists) :
    QObject(builder),
    m_builder(builder)
{
    Q_ASSERT(builder);

    m_statement = "CREATE TABLE";
    m_statement += ifNotExists ? " IF NOT EXISTS" : "";
    m_columns.clear();
}

CreateTableSqlQueryBuilder *CreateTableSqlQueryBuilder::column(const QString &name, int type, int key, bool autoIncrement)
{
    return column(name, QString(), type, key, autoIncrement);
}

CreateTableSqlQueryBuilder *CreateTableSqlQueryBuilder::column(const QString &name, const QString &defaultValue, int type, int key, bool autoIncrement)
{
    QString meta = m_builder->type(type);
    switch (key) {
    case SqlQueryBuilder::PRIMARY_KEY:
        meta += " PRIMARY KEY";
        meta += autoIncrement ? " AUTOINCREMENT" : "";
        break;
    case SqlQueryBuilder::UNIQUE_KEY:
        meta += " UNIQUE";
        break;
    case SqlQueryBuilder::KEY:
        meta += " KEY";
        break;
    }

    if (!defaultValue.isEmpty())
        meta += " DEFAULT '" + defaultValue + "'";

    m_columns.append(name + " " + meta);

    return this;
}

QString CreateTableSqlQueryBuilder::build() const
{
    return m_statement + " " + m_builder->table() + " (" + m_columns.join(",") + ")";
}

QSqlQuery CreateTableSqlQueryBuilder::buildQuery() const
{
    return QSqlQuery(build());
}

InsertSqlQueryBuilder::InsertSqlQueryBuilder(SqlQueryBuilder *builder) :
    BaseSqlQueryBuilder(builder)
{
}

QSqlQuery InsertSqlQueryBuilder::buildQuery()
{
    QVector<QString> args; args.fill("?", m_columns.size());

    QString colNames;
    foreach (QString colName, m_columns)
        colNames += colName + ",";

    QString sql = "INSERT INTO " + m_builder->table() + " (" + colNames.mid(0, colNames.size() - 1) +")"
                  " VALUES (" + QStringList().fromVector(args).join(",") + ")";

//    TtsDebug::debug("SQL QUERY: " + sql);

    QSqlQuery query(sql);
    foreach (QVariant value, m_values) {
//        TtsDebug::debug("PARAMS: " + value.toString());
        query.addBindValue(value);
    }

    return query;
}

UpdateSqlQueryBuilder::UpdateSqlQueryBuilder(SqlQueryBuilder *builder) :
    BaseSqlQueryBuilder(builder)
{

}

BaseSqlQueryBuilder *BaseSqlQueryBuilder::value(const QString &column, const QVariant &value)
{
    m_columns.append(column);
    m_values.append(value);

    return this;
}

BaseSqlQueryBuilder::BaseSqlQueryBuilder(SqlQueryBuilder *builder) :
    QObject(builder)
{
    m_builder = builder;
}

BaseSqlQueryBuilder *BaseSqlQueryBuilder::where(const QString &clause)
{
    Q_ASSERT(!m_whereClause.size());

    m_whereClause = " WHERE " + clause;

    return this;
}

BaseSqlQueryBuilder *BaseSqlQueryBuilder::andWhere(const QString &clause)
{
    Q_ASSERT(m_whereClause.size());

    m_whereClause += " AND " + clause;

    return this;
}

BaseSqlQueryBuilder *BaseSqlQueryBuilder::orWhere(const QString &clause)
{
    Q_ASSERT(m_whereClause.size());

    m_whereClause += " OR " + clause;

    return this;
}

BaseSqlQueryBuilder *BaseSqlQueryBuilder::order(const QString &column, bool ascending)
{
    if (m_order.isEmpty())
        m_order = " ORDER BY ";
    else
        m_order += ", ";

    m_order += column;
    m_order += ascending ? " ASC" : " DESC";

    return this;
}


QSqlQuery UpdateSqlQueryBuilder::buildQuery()
{
    QStringList vals;
    foreach (QString value, m_columns)
        vals << value + " = ?";

    QString sql = "UPDATE " + m_builder->table() + " SET " + vals.join(", ") + m_whereClause;
//    TtsDebug::debug("SQL QUERY: " + sql);

    QSqlQuery query(sql);
    foreach (QVariant value, m_values) {
//        TtsDebug::debug("PARAMS: " + value.toString());
        query.addBindValue(value);
    }

    return query;
}

SelectSqlQueryBuilder::SelectSqlQueryBuilder(const QString &fields, SqlQueryBuilder *builder) :
    BaseSqlQueryBuilder(builder)
{
    m_fields = fields.split(",");
}

SelectSqlQueryBuilder::SelectSqlQueryBuilder(const QStringList &fields, SqlQueryBuilder *builder) :
    BaseSqlQueryBuilder(builder)
{
    m_fields = fields;
}

QSqlQuery SelectSqlQueryBuilder::buildQuery()
{   
    QString sql = "SELECT " + m_fields.join(",") + " FROM " + m_builder->table() + " " + m_whereClause + m_order;

    return QSqlQuery(sql);
}
