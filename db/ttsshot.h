#ifndef TTSSHOT_H
#define TTSSHOT_H

#include "activerecord.h"

class TtsShot : public ActiveRecord
{
    Q_OBJECT
public:
    static const QString ID;
    static const QString ROOM;
    static const QString X;
    static const QString Y;
    static const QString TIME;
    static const QString POINTS;

    explicit TtsShot(int id = -1, QObject *parent = 0);
    explicit TtsShot(QSqlQuery &query, QObject *parent = 0);
    explicit TtsShot(int roomId, int x, int y, int time, int points, QObject *parent = 0);

    static void createTable();
    static QVector<TtsShot *> all(QObject *parent = 0);
    // method name from friends
    static QVector<TtsShot *> fromRoom(int roomId);

signals:

public slots:

    // TtsActiveRecord interface
public:
    const QStringList columns() const;
    const QString tableName() const;

    int id() const;
    int roomId() const;
    int x() const;
    int y() const;
    int time() const;
    int points() const;

protected:
    bool loadEntity(const QSqlQuery &query);
    bool createEntity();
    bool updateEntity();

private:
    int m_id;
    int m_roomId;
    int m_x;
    int m_y;
    int m_time;
    int m_points;
};

#endif // TTSSHOT_H
