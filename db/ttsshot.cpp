#include "querybuilder.h"
#include "ttsshot.h"

#include <QVector>

const QString TtsShot::ID = "id";
const QString TtsShot::ROOM = "room";
const QString TtsShot::X = "x";
const QString TtsShot::Y = "y";
const QString TtsShot::TIME = "time";
const QString TtsShot::POINTS = "points";

TtsShot::TtsShot(int id, QObject *parent) : ActiveRecord(parent)
{
    loadEntityPK(id);
}

TtsShot::TtsShot(QSqlQuery &query, QObject *parent)
{
    Q_UNUSED(parent);
    loadEntity(query);
}

TtsShot::TtsShot(int roomId, int x, int y, int time, int points, QObject *parent)
{
    Q_UNUSED(parent);
    Q_ASSERT(roomId);

    m_roomId  = roomId;
    m_x = x;
    m_y = y;
    m_time = time;
    m_points = points;

    createEntity();
}

QVector<TtsShot *> TtsShot::all(QObject *parent)
{
    QSqlQuery query = SqlQueryBuilder(TtsShot().tableName()).select()
            ->order(ID)
            ->buildQuery();

    execQuery(query, true);

    QVector<TtsShot *> items;

    while (query.next())
        items << new TtsShot(query, parent);

    return items;
}

QVector<TtsShot *> TtsShot::fromRoom(int roomId)
{
    Q_ASSERT(roomId);

    QSqlQuery query = SqlQueryBuilder(TtsShot().tableName()).select()
            ->where(ROOM + " = " + QString::number(roomId))
            ->order(ID)
            ->buildQuery();

    execQuery(query, true);

    QVector<TtsShot *> items;

    while (query.next())
        items << new TtsShot(query);

    return items;
}

const QStringList TtsShot::columns() const
{
    return {ID, ROOM, X, Y, TIME, POINTS};
}

void TtsShot::createTable()
{
    QString sql = SqlQueryBuilder(TtsShot().tableName()).createTable()
            ->column(ID, SqlQueryBuilder::INTEGER, SqlQueryBuilder::PRIMARY_KEY, true)
            ->column(ROOM, SqlQueryBuilder::INTEGER)
            ->column(X, SqlQueryBuilder::INTEGER)
            ->column(Y, SqlQueryBuilder::INTEGER)
            ->column(TIME, SqlQueryBuilder::INTEGER)
            ->column(POINTS, SqlQueryBuilder::INTEGER)
            ->build();

    execQuery(sql, true);
}

bool TtsShot::loadEntity(const QSqlQuery &query)
{
    m_id = query.value(0).toInt();
    m_roomId = query.value(1).toInt();
    m_x = query.value(2).toInt();
    m_y = query.value(3).toInt();
    m_time = query.value(4).toInt();
    m_points = query.value(5).toInt();

    return true;
}

bool TtsShot::createEntity()
{
    QSqlQuery query = SqlQueryBuilder(tableName()).insert()
            ->value(ROOM, m_roomId)
            ->value(X, m_x)
            ->value(Y, m_y)
            ->value(TIME, m_time)
            ->value(POINTS, m_points)
            ->buildQuery();

    if (execQuery(query, true)) {
        m_id = query.lastInsertId().toInt();
        return true;
    }

    return false;
}

bool TtsShot::updateEntity()
{
    QSqlQuery query = SqlQueryBuilder(tableName()).insert()
            ->value(ROOM, m_roomId)
            ->value(X, m_x)
            ->value(Y, m_y)
            ->value(TIME, m_time)
            ->value(POINTS, m_points)
            ->where(ID + " = " + QString::number(m_id))
            ->buildQuery();

    return execQuery(query, true);
}

int TtsShot::points() const
{
    return m_points;
}

int TtsShot::time() const
{
    return m_time;
}

int TtsShot::y() const
{
    return m_y;
}

int TtsShot::x() const
{
    return m_x;
}

int TtsShot::roomId() const
{
    return m_roomId;
}

int TtsShot::id() const
{
    return m_id;
}


const QString TtsShot::tableName() const
{
    return "shot";
}
