#ifndef TTSUSER_H
#define TTSUSER_H

#include "activerecord.h"

#include <QSqlQuery>
#include <QVector>

class TtsUser : public ActiveRecord
{
public:
    enum {
        STATE_ACTIVE,
        STATE_DISABLED,
        STATE_DELETED
    };

    static const QString ID;
    static const QString NAME;
    static const QString TRAININGS;
    static const QString SCORES;
    static const QString STATE;

    TtsUser(const TtsUser &&);
    TtsUser(int id = -1, QObject *parent = 0);
    TtsUser(QSqlQuery &query, QObject *parent = 0);
    TtsUser(const QString &name, int state = STATE_ACTIVE, QObject *parent = 0);

    static void createTable();
    static QVector<TtsUser *> all(QObject *parent = 0);
    static QVector<TtsUser *> all(int state, QObject *parent = 0);
    static QMap<int, TtsUser *> map(QObject *parent = 0);
    static TtsUser *find(const QString &name, QObject *parent);
    static TtsUser find(const QString &name);

    int id() const;
    QString name() const;
    int trainings() const;
    qreal scores() const;
    int state() const;
    bool active() const;
    bool deleted() const;

    void setName(const QString &name);
    void setState(int state);
    void remove();
    void addTrainingResult(qreal scores);

protected:
    int m_id = 0;
    QString m_name = "NO NAME USER";
    int m_trainings = 0;
    qreal m_scores = 0;
    int m_state = STATE_ACTIVE;

    virtual bool loadEntity(const QSqlQuery &query);
    virtual bool createEntity();
    virtual bool updateEntity();

    // TtsActiveRecord interface
protected:
    const QStringList columns() const;

    // TtsActiveRecord interface
public:
    const QString tableName() const;
};

#endif // TTSUSER_H
