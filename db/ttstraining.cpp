#include "querybuilder.h"
#include "ttstraining.h"

#include <QVariant>

#include <tts/ttsdebug.h>

const QString TtsTraining::ID = "id";
const QString TtsTraining::NAME = "name";
const QString TtsTraining::TIME = "time";
const QString TtsTraining::DURATION = "duration";

TtsTraining::TtsTraining(int id, QObject *parent) : ActiveRecord(parent)
{
    loadEntityPK(id);
}

TtsTraining::TtsTraining(QSqlQuery &query, QObject *parent) : ActiveRecord(parent)
{
    loadEntity(query);
}

TtsTraining::TtsTraining(const QString &name, qint64 start, qint64 duration)
{
    m_name = name;
    m_time = start;
    m_duration = duration;

    createEntity();
}

const QVector<TtsTraining *> TtsTraining::all(QObject *parent)
{
    QSqlQuery query = SqlQueryBuilder(TtsTraining().tableName()).select()
            ->order(ID)
            ->buildQuery();

    execQuery(query, true);

    QVector<TtsTraining *> items;

    while (query.next())
        items << new TtsTraining(query, parent);

    return items;
}

int TtsTraining::id() const
{
    return m_id;
}

const QString TtsTraining::name() const
{
    return m_name;
}

TtsTraining *TtsTraining::setName(const QString &name)
{
    m_name = name;
    emit changed();

    return this;
}

qint64 TtsTraining::timestamp() const
{
    return m_time;
}

qint64 TtsTraining::duration() const
{
    return m_duration;
}

QDateTime TtsTraining::datetime() const
{
    return QDateTime::fromMSecsSinceEpoch(m_time);
}

void TtsTraining::createTable()
{
    QString sql = SqlQueryBuilder(TtsTraining().tableName()).createTable(true)
            ->column(ID, SqlQueryBuilder::INTEGER, SqlQueryBuilder::PRIMARY_KEY, true)
            ->column(NAME, SqlQueryBuilder::INTEGER)
            ->column(TIME, SqlQueryBuilder::INTEGER)
            ->column(DURATION, SqlQueryBuilder::INTEGER)
            ->build();

    execQuery(sql, true);
}

bool TtsTraining::loadEntity(const QSqlQuery &query)
{
    m_id = query.value(ID).toInt();
    m_name = query.value(NAME).toString();
    m_time = query.value(TIME).toLongLong();
    m_duration = query.value(DURATION).toLongLong();

    return true;
}

bool TtsTraining::updateEntity()
{
    QSqlQuery query = SqlQueryBuilder(tableName()).update()
            ->value(NAME, m_name)
            ->where(ID + " = " + QString::number(m_id))
            ->buildQuery();

    return execQuery(query, true);
}

/*!
 * \brief TtsTraining::createEntity
 * \return
 * Error corrections from friends
 */
bool TtsTraining::createEntity()
{
    QSqlQuery query = SqlQueryBuilder(tableName()).insert()
            ->value(NAME, m_name)
            ->value(TIME, m_time)
            ->value(DURATION, m_duration)
            ->buildQuery();

    if (execQuery(query, true)) {
        m_id = query.lastInsertId().toInt();
        return true;
    }

    return false;
}

const QStringList TtsTraining::columns() const
{
    return {ID, NAME, TIME, DURATION};
}

const QString TtsTraining::tableName() const
{
    return "training";
}
